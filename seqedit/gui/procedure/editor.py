"""Procedure root view."""

from gui.view import EditorView
from PySide6 import QtWidgets


class ProcedureEditor(EditorView):
    """Simple view of the procedure information."""

    def __init__(self, obj, parent=None):
        """Create view."""
        EditorView.__init__(self, obj, parent)
        layout = QtWidgets.QFormLayout(self)
        label = QtWidgets.QLabel("<h2>Procedure Information</h2>")
        layout.addWidget(label)
        layout.addWidget(QtWidgets.QLabel("<b>Name</b>"))
        text_edit = QtWidgets.QTextEdit(self.__procedure__.name)
        layout.addWidget(text_edit)
        self.setLayout(layout)

        text_edit.textChanged.connect(self.__name_changed__)
        self.__name_edit__ = text_edit

    def __name_changed__(self):
        """Change procedure name."""
        text = self.__name_edit__.document().toPlainText()
        self.__procedure__.name = text
