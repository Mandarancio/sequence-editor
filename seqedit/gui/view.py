"""Basic class of a editor plugin."""
from core.procedure import Node, Procedure
from gui.graph.scene import NodeScene
from gui.graph.view import View
from PySide6 import QtCore, QtWidgets


class EditorView(QtWidgets.QWidget):
    """Basic editing/viewing widget."""

    open_element = QtCore.Signal(str, str)

    def __init__(self, obj, parent=None):
        """
        Create the editor View.

        Arguments:
        ---------
            obj: object to be visualized or edited (e.g. a procedure)
            parent: parent widget

        """
        QtWidgets.QWidget.__init__(self, parent)
        if isinstance(obj, Procedure):
            self.__procedure__ = obj
            self.__sequence__ = None
        elif isinstance(obj, Node):
            self.__procedure__ = obj.procedure
            self.__sequence__ = obj
        else:
            self.__procedure__ = None
            self.__sequence__ = None

    def initialize_view(self):
        """Finalize the initialization of the view."""


class GraphEditorView(EditorView):
    """Graph editor view."""

    def __init__(self, obj, event_filter=None, parent=None):
        """
        Create graph editor view.

        Arguments:
        ---------
            obj: object to be visualized.
            event_filter: object that manage ux events.
            parent: parent widget.

        """
        EditorView.__init__(self, obj, parent)

        main_layout = QtWidgets.QHBoxLayout()
        main_layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(main_layout)

        splitter = QtWidgets.QSplitter(self)

        self.scene = NodeScene()
        self.scene.setSceneRect(0, 0, 15000, 15000)
        self.view = View(splitter)
        self.view.setScene(self.scene)

        main_layout.addWidget(splitter)

        self.__sidebar__ = QtWidgets.QWidget(splitter)
        self.__sidebar__.setMinimumWidth(300)

        sidebar_layout = QtWidgets.QVBoxLayout(self.__sidebar__)
        # sidebar_layout.setMargin(0)
        self.__sidebar__.setLayout(sidebar_layout)
        splitter.setSizes([1, 0])
        self.__side_layout__ = sidebar_layout
        self.__side_widget__ = None
        self.__splitter__ = splitter

        self.__event_filter__ = event_filter
        # connect
        if event_filter:
            event_filter.install(self.scene)
            event_filter.open_editor.connect(self.__open_editor__)
            event_filter.hide_editor.connect(self.__hide_editor__)

        self.view.item_context_menu.connect(self.__item_context_menu__)
        self.view.context_menu.connect(self.__context_menu__)

    def initialize_view(self):
        """Initialize view."""
        self.__load_sequence__()

    def __load_sequence__(self):
        """Load sequence."""

    def __open_editor__(self):
        """Open side editor."""

    def __hide_editor__(self):
        """Hide node editor from the sidebar."""
        if self.__side_widget__ is not None:
            self.__side_widget__.setEnabled(False)
        self.update()

    def __context_menu__(self, _):
        """Create view context menu."""

    @staticmethod
    def __item_context_menu__(item, pos):
        """Create item context menu."""
        if hasattr(item, "context_menu"):
            item.context_menu(pos)
