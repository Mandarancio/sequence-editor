"""Simple UI dark palette."""

from PySide6.QtCore import Qt
from PySide6.QtGui import QColor, QPalette


def dark_palette():
    """Create a simple dark palette for QT5."""
    palette = QPalette()
    palette.setColor(QPalette.Window, QColor(53, 58, 63))
    palette.setColor(QPalette.WindowText, Qt.white)
    palette.setColor(QPalette.Base, QColor(30, 35, 40))
    palette.setColor(QPalette.AlternateBase, QColor(53, 58, 63))
    palette.setColor(QPalette.ToolTipBase, Qt.black)
    palette.setColor(QPalette.ToolTipText, Qt.white)
    palette.setColor(QPalette.Text, Qt.white)
    palette.setColor(QPalette.Button, QColor(53, 58, 63))
    palette.setColor(QPalette.ButtonText, Qt.white)
    palette.setColor(QPalette.BrightText, Qt.red)
    palette.setColor(QPalette.Link, QColor(42, 130, 218))
    palette.setColor(QPalette.Highlight, QColor(42, 130, 218))
    palette.setColor(QPalette.HighlightedText, Qt.black)
    return palette
