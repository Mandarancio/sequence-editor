"""Procedure Plugin view."""
from core.procedure import Plugin
from gui.view import EditorView
from PySide6 import QtGui, QtWidgets


class PluginsEditor(EditorView):
    """Simple plugin list editor."""

    def __init__(self, obj, parent=None):
        """Create the editor."""
        EditorView.__init__(self, obj, parent)

        layout = QtWidgets.QVBoxLayout(self)
        self.setLayout(layout)
        list_view = QtWidgets.QListView(self)
        model = QtGui.QStandardItemModel(0, 1, self)
        list_view.setModel(model)
        for plugin in self.__procedure__.plugins:
            item = QtGui.QStandardItem(plugin.lib)
            item.setData(plugin)
            model.appendRow(item)

        layout.addWidget(list_view)

        bottom = QtWidgets.QWidget(self)
        bottom.setMaximumHeight(30)
        blayout = QtWidgets.QHBoxLayout(bottom)
        # blayout.setMargin(1)
        bottom.setLayout(blayout)

        add_btn = QtWidgets.QToolButton()
        add_btn.setText("+")
        rmv_btn = QtWidgets.QToolButton()
        rmv_btn.setText("-")

        blayout.addWidget(add_btn)
        blayout.addWidget(rmv_btn)
        blayout.addStretch()

        layout.addWidget(bottom)

        add_btn.clicked.connect(self.__add_plugin__)
        rmv_btn.clicked.connect(self.__rmv_plugin__)

        self.__list__ = list_view

    def __add_plugin__(self):
        """Add a new plugin to the procedure."""
        text, ok = QtWidgets.QInputDialog.getText(
            self,
            "Add plugin",
            "Plugin name:",
            QtWidgets.QLineEdit.Normal,
        )
        if ok and text:
            plugin = Plugin(text)
            self.__procedure__.add_plugin(plugin)
            item = QtGui.QStandardItem(text)
            item.setData(plugin)
            self.__list__.model().appendRow(item)

    def __rmv_plugin__(self):
        """Remove selected plugin from procedure."""
        index = self.__list__.selectedIndexes()
        if len(index) == 1:
            index = index[0]
            item = self.__list__.model().itemFromIndex(index)

            text = f"Are you sure to remove the plugin `{item.text()}`?"
            res = QtWidgets.QMessageBox.question(
                self,
                "Remove plugin",
                text,
            )
            if res == QtWidgets.QMessageBox.StandardButton.Yes:
                self.__procedure__.remove_plugin(item.data())
                self.__list__.model().removeRow(index.row())
