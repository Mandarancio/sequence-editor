"""File and sequence view."""
import os

from core.procedure import Node, Procedure, Root, make_node
from gui.palette import dark_palette
from gui.section import Section
from lxml import etree
from PySide6 import QtCore, QtGui, QtWidgets


class SeqView(QtWidgets.QTreeView):
    """Sequence Tree View."""

    new_sequence = QtCore.Signal()
    remove_sequence = QtCore.Signal()
    close_procedure = QtCore.Signal()

    def __init__(self, parent=None):
        """Initialize the sequence view."""
        QtWidgets.QTreeView.__init__(self, parent=parent)
        model = QtGui.QStandardItemModel(0, 1)
        model.setHorizontalHeaderLabels(["Sequences"])
        self._seqs = {}
        self.setMinimumWidth(200)
        self.setModel(model)
        self.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self._items = {}
        self._model = model
        self.__root_fg__ = QtGui.QBrush(QtCore.Qt.red)
        self.__normal_fg__ = dark_palette().text()

        self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.__show_menu__)

    def __show_menu__(self, position):
        """Show context menu."""
        index = self.indexAt(position)
        item = self.model().itemFromIndex(index)
        if item:
            data = item.data()
            if isinstance(data, Node):
                self.__menu_node__(data, position)
            elif isinstance(data, Procedure):
                self.__menu_procedure__(data, position)

    def __menu_node__(self, _, position):
        """Show node menu."""
        pos = self.viewport().mapToGlobal(position)
        menu = QtWidgets.QMenu(self)
        menu.addAction("Remove sequence")
        action = menu.exec_(pos)
        if action:
            if action.text() == "Remove sequence":
                self.remove_sequence.emit()

    def __menu_procedure__(self, _, position):
        """Show procedure menu."""
        pos = self.viewport().mapToGlobal(position)
        menu = QtWidgets.QMenu(self)
        menu.addAction("Add sequence")
        menu.addAction("Close procedure")
        action = menu.exec_(pos)
        if action:
            if action.text() == "Add sequence":
                self.new_sequence.emit()
            elif action.text() == "Close procedure":
                self.close_procedure.emit()

    def set_sequences(self, path, tree):
        """Clear and initialize a new set of sequences."""
        sequences = tree.sequences
        self._seqs[path] = sequences
        root = QtGui.QStandardItem(path)
        root.setData(tree)
        self._items[(path, None)] = root
        for elem in sequences:
            node = sequences[elem]
            item = QtGui.QStandardItem(elem)
            item.setData(node)
            if node.is_root:
                item.setForeground(self.__root_fg__)
            self._items[(path, elem)] = item
            node.updated.connect(self.__update_items__)
            root.appendRow(item)
        self._model.appendRow(root)
        self.expand(self._model.indexFromItem(root))

    def __update_items__(self):
        """Update element in list."""
        to_change = []
        for key, item in self._items.items():
            if key[1] and item:
                # is a node and not procedure.
                node = item.data()
                if node.name != key[1]:
                    to_change.append((key, (key[0], node.name)))
                    item.setText(node.name)
                if node.is_root:
                    item.setForeground(self.__root_fg__)
                else:
                    item.setForeground(self.__normal_fg__)
        for key0, key1 in to_change:
            item = self._items[key0]
            del self._items[key0]
            self._items[key1] = item

    def find(self, path, seqname):
        """Search item by path and name."""
        key = (path, seqname)
        if key in self._items:
            return self._items[key]
        return None

    def select(self, key):
        """Select item by index."""
        if key is not None and key in self._items:
            index = self._model.indexFromItem(self._items[key])
            self.selectionModel().setCurrentIndex(
                index, QtCore.QItemSelectionModel.SelectCurrent
            )

    def item(self, index):
        """Get item at a given index."""
        return self.model().itemFromIndex(index)

    def unselect(self):
        """Unselect item."""
        self.selectionModel().reset()

    def add_sequence(self, procedure, sequence):
        """Add a new sequence to the list."""
        root_item = self.find(procedure.path, None)
        if root_item:
            item = QtGui.QStandardItem(sequence.name)
            item.setData(sequence)
            if sequence.is_root:
                item.setForeground(self.__root_fg__)
            self._items[(procedure.path, sequence.name)] = item
            sequence.updated.connect(self.__update_items__)
            root_item.appendRow(item)

    def del_sequence(self, sequence):
        """Remove a sequence from the list."""
        root_item = self.find(sequence.procedure.path, None)
        child_item = self.find(sequence.procedure.path, sequence.name)
        if root_item and child_item:
            root_index = self._model.indexFromItem(root_item)
            child_index = self._model.indexFromItem(child_item)
            self._model.removeRow(child_index.row(), root_index)

    def remove_procedure(self, procedure):
        """Remove procedure from tree."""
        root_item = self.find(procedure.path, None)
        index = self._model.indexFromItem(root_item)
        self._model.removeRow(index.row())


class SeqManager(QtWidgets.QWidget):
    """Files Tree Manager and widget."""

    def __init__(self, parent=None):
        """Create widget."""
        QtWidgets.QWidget.__init__(self, parent=parent)
        layout = QtWidgets.QHBoxLayout(self)
        # layout.setMargin(1)
        split = QtWidgets.QSplitter()

        self.setLayout(layout)
        layout.addWidget(split)
        self._seq_list = SeqView(parent=split)
        self._editors = {}
        main_widget = QtWidgets.QWidget(split)
        main_layout = QtWidgets.QGridLayout(main_widget)
        # main_layout.setMargin(0)
        self._layout = main_layout
        main_widget.setLayout(main_layout)

        main_widget.setMinimumWidth(800)
        self._seq_list.setMaximumWidth(400)

        split.addWidget(main_widget)
        self._current = None
        self._current_key = None
        model = self._seq_list.selectionModel()
        model.currentChanged.connect(self.__changed__)

        self._seq_list.close_procedure.connect(self.close_procedure)
        self._seq_list.new_sequence.connect(self.new_sequence)
        self._seq_list.remove_sequence.connect(self.remove_sequence)

    def __select__(self, path, name):
        """Slot used to manage the selection of a sequence."""
        if path == "":
            path = self._current_key[0]
        else:
            path = os.path.join(os.path.split(self._current_key[0])[:-1], path)
        self._seq_list.select((path, name))

    def open(self, obj):
        """Open sequence or procedure."""
        if obj is None:
            return None
        editor = self.__try_existing__(obj)
        if editor is None:
            editor = Section(obj, self)
            self._current = editor
            self._editors[obj] = editor
            self._current_key = obj
            self._layout.addWidget(editor)
        return editor

    def open_from_path(self, path, seqname):
        """Try to open a sequence from a path."""
        if not path:
            path = self._current_key.path
        if self._seq_list.find(path, seqname):
            self._seq_list.select((path, seqname))
        else:
            print("!!", path, seqname, "not found")

    def __try_existing__(self, key):
        """Try to open existing view."""
        if self._current is not None:
            self._current.hide()
        if key in self._editors:
            editor = self._editors[key]
            self._current = editor
            self._current_key = key
            editor.show()
            return editor
        return None

    def __current_procedure__(self):
        """Get current procedure."""
        key = self._current_key
        if key is None:
            return None
        if isinstance(key, Procedure):
            return key
        return key.procedure

    def new_procedure(self):
        """Create a new procedure."""
        path = QtWidgets.QFileDialog.getSaveFileName(
            caption="New procedure",
            filter="xml files (*.xml)",
            parent=self,
        )[0]
        if path:
            path = os.path.relpath(path)
            procedure = Procedure(
                "new procedure",
                workspace=[],
                plugins=[],
                sequences={},
            )
            procedure.path = path
            self.load_procedure(path, procedure)

    def close_procedure(self):
        """Close current procedure."""
        procedure = self.__current_procedure__()
        if procedure:
            self._seq_list.remove_procedure(procedure)

    def new_sequence(self):
        """Create new sequence."""
        procedure = self.__current_procedure__()
        if procedure:
            sequence = Root(make_node("Sequence", procedure))
            sequence.procedure = procedure
            procedure.add_sequence(sequence)
            self._seq_list.add_sequence(procedure, sequence)

    def remove_sequence(self):
        """Remove current sequence from procedure."""
        if self._current_key and isinstance(self._current_key, Root):
            sequence = self._current_key
            self._seq_list.del_sequence(sequence)
            sequence.procedure.remove_sequence(sequence)

    def save(self):
        """Save current procedure as same file."""
        key = self._current_key
        procedure = None
        if isinstance(key, Procedure):
            procedure = key
        else:
            procedure = key.procedure
        xml = procedure.xml()
        tree = etree.ElementTree(xml)
        tree.write(key[0], pretty_print=True)

    def save_as(self):
        """Save current procedure as new file."""
        key = self._current_key
        if key is not None:
            path = QtWidgets.QFileDialog.getSaveFileName(
                caption="Save procedure as",
                filter="xml files (*.xml)",
                parent=self,
            )[0]
            if path:
                procedure = None
                if isinstance(key, Procedure):
                    procedure = key
                else:
                    procedure = key.procedure
                procedure.path = path
                xml = procedure.xml()
                tree = etree.ElementTree(xml)
                tree.write(path, pretty_print=True)

    def load_procedure(self, path, tree: Procedure):
        """Load procedure (dict of sequences)."""
        self._seq_list.unselect()
        self._seq_list.set_sequences(path, tree)
        self._seq_list.select((path, None))
        return self._current

    def __changed__(self, current, _):
        """Slot used to manage the tree selection change."""
        item = self._seq_list.item(current)
        if item:
            obj = item.data()
            self.open(obj)
