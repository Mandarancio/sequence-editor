"""Multi node  type editor."""
import os

from core import node_info as info
from PySide6 import QtCore, QtGui, QtWidgets


class OptionalWidget(QtWidgets.QWidget):
    """Widget for optional arguments."""

    changed = QtCore.Signal(bool)

    def __init__(self, inner: QtWidgets.QWidget, parent=None):
        """Create the optional widget for the given widget."""
        QtWidgets.QWidget.__init__(self, parent=parent)
        layout = QtWidgets.QHBoxLayout(self)
        self.setLayout(layout)
        chbox = QtWidgets.QCheckBox(self)
        layout.addWidget(chbox)
        layout.addWidget(inner)
        self.__inner__ = inner
        self.__chbox__ = chbox
        self.setMinimumHeight(28)
        # layout.setMargin(1)
        self.__current__ = chbox.isChecked()
        self.__change__(chbox.isChecked())
        chbox.stateChanged.connect(self.__change__)

    def __change__(self, checked):
        """Slot to manage the optional checkbox."""
        self.__inner__.setEnabled(checked)
        if self.__current__ != bool(checked):
            self.__current__ = checked
            if checked:
                self.__emit_value__()
            self.changed.emit(checked)

    def __emit_value__(self):
        """Publish inner value."""
        inner = self.__inner__
        if isinstance(inner, (QtWidgets.QSpinBox, QtWidgets.QDoubleSpinBox)):
            inner.valueChanged.emit(inner.value())
        elif isinstance(inner, QtWidgets.QCheckBox):
            inner.stateChanged.emit(inner.isChecked())
        elif isinstance(inner, QtWidgets.QComboBox):
            inner.textActivated.emit(inner.currentText())
        elif isinstance(inner, QtWidgets.QLineEdit):
            inner.textChanged.emit(inner.text())
        elif isinstance(inner, FileWidget):
            inner.file_changed.emit(inner.file_path)

    def set_used(self):
        """Enable widgets."""
        self.__current__ = True
        self.__chbox__.setChecked(True)
        self.__inner__.setEnabled(True)


class FileWidget(QtWidgets.QWidget):
    """Custom file edit widget."""

    # path
    file_changed = QtCore.Signal(str)

    def __init__(self, parent=None):
        """Create file widget."""
        QtWidgets.QWidget.__init__(self, parent=parent)
        layout = QtWidgets.QHBoxLayout(self)
        # layout.setMargin(0)
        self.setLayout(layout)
        self._label = QtWidgets.QLineEdit(self)
        self._button = QtWidgets.QToolButton(self)
        self._button.setMaximumWidth(28)
        self._button.setIcon(QtGui.QIcon.fromTheme("document-open"))
        self._button.clicked.connect(self.__open_file__)
        # add widgets
        layout.addWidget(self._label)
        layout.addWidget(self._button)

    def __open_file__(self):
        """Slot used to manage the open button action."""
        print("open dialog")
        path = QtWidgets.QFileDialog.getOpenFileName(
            caption="Open Procedure file", filter="xml files (*.xml)"
        )
        if path is not None and path[0]:
            path = os.path.relpath(path[0])
            self._label.setText(path)
            self.file_changed.emit(path)

    def set_path(self, path: str):
        """Set path from string."""
        self._label.setText(path)

    @property
    def file_path(self):
        """Get file path."""
        return self._label.text()


class EditAnythingWidget(QtWidgets.QWidget):
    """Multipurpouse Edit widget."""

    # field_name, value
    value_changed = QtCore.Signal(str, object)
    # field_name
    disable_field = QtCore.Signal(str)

    def __init__(self, title, data, model, parent=None):
        """Initialize Edit widget using node informations."""
        QtWidgets.QWidget.__init__(self, parent=parent)
        layout = QtWidgets.QFormLayout(self)
        # layout.setMargin(3)
        self.setLayout(layout)
        self.setMinimumSize(100, 100)
        self._n_type = title
        self._descriptor = model
        self._args = data
        self.__fn_lut__ = {
            "int": self.__int_w__,
            "float": self.__float_w__,
            "bool": self.__bool_w__,
            "choice": self.__choice_w__,
            "file": self.__file_w__,
            "str": self.__str_w__,
        }
        title = QtWidgets.QLabel(f"<h3>{title}</h3>", self)
        title.setMaximumHeight(30)
        title.setMinimumHeight(28)
        layout.addRow(title)

    def init_gui(self):
        """Intialize the UI."""
        if self._descriptor is None:
            return
        for argname in self._descriptor:
            label = QtWidgets.QLabel(argname, self)
            value = self._args[argname] if argname in self._args else None
            widget = self.__create_widget__(self._descriptor[argname], value)
            self.layout().addRow(label, widget)

    def __int_w__(
        self,
        arg: info.NodeTypeInfo,
        value: object,
        slot: callable,
    ) -> QtWidgets.QWidget:
        """Create integer edit widget."""
        widget = QtWidgets.QSpinBox(self)
        if arg.has_range:
            widget.setRange(arg.min, arg.max)
        if value is not None:
            widget.setValue(int(value))
        widget.valueChanged.connect(slot)
        return widget

    def __float_w__(
        self,
        arg: info.NodeTypeInfo,
        value: object,
        slot: callable,
    ) -> QtWidgets.QWidget:
        """Create float edit widget."""
        widget = QtWidgets.QDoubleSpinBox(self)
        if arg.has_range:
            widget.setRange(arg.max, arg.max)
        if value is not None:
            widget.setValue(float(value))
        widget.valueChanged.connect(slot)
        return widget

    def __bool_w__(
        self,
        arg: info.NodeTypeInfo,
        value: object,
        _: callable,
    ) -> QtWidgets.QWidget:
        """Create bool edit widget."""
        widget = QtWidgets.QCheckBox(self)
        if value is not None:
            widget.setChecked(bool(value))
        widget.stateChanged.connect(
            lambda value: self.value_changed.emit(arg.name, bool(value))
        )
        return widget

    def __choice_w__(
        self,
        arg: info.NodeTypeInfo,
        value: object,
        slot: callable,
    ) -> QtWidgets.QWidget:
        """Create choice edit widget."""
        widget = QtWidgets.QComboBox(self)
        for item in arg.choices:
            widget.addItem(item)
        if value is not None:
            widget.setCurrentText(value)
        widget.textActivated.connect(slot)
        return widget

    def __file_w__(
        self,
        _: info.NodeTypeInfo,
        value: object,
        slot: callable,
    ) -> QtWidgets.QWidget:
        """Create file edit widget."""
        widget = FileWidget(self)
        if value is not None:
            widget.set_path(value)
        widget.file_changed.connect(slot)
        return widget

    def __str_w__(
        self,
        _: info.NodeTypeInfo,
        value: object,
        slot: callable,
    ) -> QtWidgets.QWidget:
        """Create string edit widget."""
        widget = QtWidgets.QLineEdit(self)
        if value is not None:
            widget.setText(value)
        widget.textChanged.connect(slot)
        return widget

    def __create_widget__(
        self,
        arg: info.NodeTypeInfo,
        value: object,
    ) -> QtWidgets.QWidget:
        """Create widget depending from info."""

        def reemit(value):
            self.value_changed.emit(arg.name, value)

        def disalble_filter_slot(value):
            if not value:
                self.disable_field.emit(arg.name)

        if arg.type_name in self.__fn_lut__:
            widget = self.__fn_lut__[arg.type_name](
                arg,
                value,
                reemit,
            )
        else:
            widget = self.__str_w__(arg, value, reemit)
        if arg.is_optional:
            inner = widget
            widget = OptionalWidget(inner, self)
            if value is not None:
                widget.set_used()
            widget.changed.connect(disalble_filter_slot)
        return widget


class NodeEditWidget(EditAnythingWidget):
    """Multipurpouse Node Edit widget."""

    def __init__(self, node, tree, parent=None):
        """Initialize Edit widget using node informations."""
        EditAnythingWidget.__init__(
            self, node.ntype, node.args, info.node_info(node).arguments, parent
        )
        self.__node__ = node
        self.__tree__ = tree
        self.__fn_lut__["param"] = self.__param_w__
        self.value_changed.connect(self.__update_node__)
        self.disable_field.connect(self.__remove_field__)

    def __param_w__(
        self,
        _: info.NodeTypeInfo,
        value: object,
        slot: callable,
    ) -> QtWidgets.QWidget:
        """Create param edit widget."""
        widget = QtWidgets.QComboBox(self)
        for param in self.__tree__.workspace:
            widget.addItem(param.name)
        if value is not None:
            widget.setCurrentText(value)
        widget.textActivated.connect(slot)
        return widget

    def __update_node__(self, field, value):
        """Update node settings."""
        self.__node__.update_field(field, value)

    def __remove_field__(self, field):
        """Remove node setting filed."""
        self.__node__.remove_field(field)


def edit(node, parent=None):
    """Open edit dialog."""
    edit_w = NodeEditWidget(node, node.procedure, parent=parent)
    edit_w.init_gui()
    return edit_w


def edit_variable(variable, parent=None):
    """Open edit dialog for variable."""
    title = f"{variable.name} ({variable.vtype})"
    data = variable.args
    descriptor = info.variable_info(variable)
    edit_w = EditAnythingWidget(title, data, descriptor, parent)
    edit_w.init_gui()
    edit_w.value_changed.connect(variable.update_field)
    return edit_w


if __name__ == "__main__":
    from collections import namedtuple

    app = QtWidgets.QApplication([])
    Node = namedtuple("Node", ["ntype"])
    editw = edit(Node("Repeat"), None)
    editw.show()
    app.exec_()
