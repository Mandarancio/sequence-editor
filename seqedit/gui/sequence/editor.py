"""Main widgets for the sequence view."""
from gui.sequence import elements
from gui.view import GraphEditorView
from PySide6 import QtGui
from PySide6.QtCore import Qt


class SequenceEditor(GraphEditorView):
    """Main widget for the sequence view mode."""

    __delta_x__ = 50
    __delta_y__ = 60
    __orig_y__ = 50

    white = QtGui.QColor(255, 255, 255)

    def __init__(self, sequence, parent=None):
        """Create view widget."""
        GraphEditorView.__init__(self, sequence, None, parent)

    def __create_view__(self):
        """Create view."""
        self.scene.clear()
        start = elements.create_start()
        start.setPos(25, self.__orig_y__)
        end = elements.create_end()
        end.setPos(50, self.__orig_y__)
        self.scene.addItem(start)
        if self.__sequence__.leafs:
            last, _ = self.create_node(
                start,
                self.__sequence__.leafs[0],
                (
                    start.width / 2 + 25 + self.__delta_x__ / 2,
                    self.__orig_y__,
                ),
            )
            end.add_item(last)
        else:
            end.add_item(start)
        self.__setup_end__(end)
        self.update()
        return start

    def __load_sequence__(self):
        """Load sequence overload."""
        start = self.__create_view__()
        self.view.centerOn(start)

    def __create_action__(self, parent, node, pos, color=Qt.white):
        """Create action node."""
        item = elements.create_node(node)
        item.setPos(pos[0], pos[1])
        self.scene.addItem(item)
        self.create_connection(parent, item, color)
        return item, (pos[0] + item.width, pos[1])

    def __create_label__(self, parent, text, pos):
        """Crate label item."""
        item = elements.create_label(text)
        item.setPos(pos[0], pos[1])
        self.scene.addItem(item)
        self.create_connection(parent, item, Qt.white)
        return item, (pos[0] + item.width, pos[1])

    def __create_parallel__(self, parent, node, pos, color):
        """Create parallel control node."""
        items = []
        max_x = 0
        for leaf in node.leafs:
            element, delta = self.create_node(parent, leaf, pos, color)
            pos = (pos[0], delta[1] + self.__delta_y__)
            max_x = max_x if delta[0] < max_x else delta[0]
            items.append(element)
        item = elements.placeholder()
        item.setPos(max_x + self.__delta_x__, parent.pos().y())
        self.scene.addItem(item)
        for itm in items:
            self.create_connection(itm, item)
        return item, (item.x(), pos[1] - self.__delta_y__)

    def __create_choice__(self, parent, node, pos, color):
        """Create choice node."""
        item = elements.create_choice(node)
        item.setPos(pos[0], pos[1])
        pos = (pos[0] + item.width + 15, pos[1])
        self.scene.addItem(item)
        self.create_connection(parent, item, Qt.white)
        items = []
        max_x = 0
        for leaf in node.leafs:
            name = leaf.name
            label, lpos = self.__create_label__(item, name, pos)
            element, delta = self.create_node(label, leaf, lpos, color)
            pos = (pos[0], delta[1] + self.__delta_y__)
            max_x = max_x if delta[0] < max_x else delta[0]
            items.append(element)
        item = elements.placeholder()
        item.setPos(max_x + self.__delta_x__, parent.pos().y())
        self.scene.addItem(item)
        for itm in items:
            self.create_connection(itm, item)
        return item, (item.x(), pos[1] - self.__delta_y__)

    def __create_fallback__(self, parent, node, pos, _):
        """Create fallback node."""
        normal = node.leafs[0]
        fallback = node.leafs[1]
        nm_item, nm_pos = self.create_node(parent, normal, pos)
        fb_item, fb_pos = self.create_node(
            parent,
            fallback,
            (pos[0], pos[1] + self.__delta_y__),
            color=QtGui.QColor(255, 128, 128),
        )
        fb_x = fb_pos[0] + self.__delta_x__
        nm_x = nm_pos[0] + self.__delta_x__
        max_x = fb_x if fb_x > nm_x else nm_x
        item = elements.placeholder()
        item.setPos(max_x, parent.pos().y())
        self.scene.addItem(item)
        self.create_connection(nm_item, item)
        self.create_connection(fb_item, item)
        return item, (max_x, pos[1] + self.__delta_y__)

    def __create_sequence__(self, parent, node, pos, color):
        """Create sequence node."""
        prev = parent
        pos_y = pos[1]
        for leaf in node.leafs:
            item, pos = self.create_node(prev, leaf, (pos[0], pos_y), color)
            prev = item
            pos = (pos[0] + self.__delta_x__, pos[1])
        return prev, (pos[0] - self.__delta_x__, pos[1])

    def __create_control__(self, parent, node, pos, color=Qt.white):
        """Create control node."""
        if node.ntype == "ParallelSequence":
            return self.__create_parallel__(parent, node, pos, color)
        if node.ntype == "Choice":
            return self.__create_choice__(parent, node, pos, color)
        if node.ntype == "Fallback" and len(node.leafs) == 2:
            return self.__create_fallback__(parent, node, pos, color)
        return self.__create_sequence__(parent, node, pos, color)

    def __create_repeat__(self, parent, node, pos, color):
        """Create repeat node."""
        start = elements.placeholder(pos[0], pos[1])

        self.scene.addItem(start)
        self.create_connection(parent, start)

        start_pos = pos
        pos = (pos[0] + 15, pos[1])
        parent = start
        if node.leafs:
            parent, pos = self.create_node(
                parent,
                node.leafs[0],
                pos,
                color,
            )
        pos = (pos[0] + 15, pos[1])
        end_pos = (pos[0], start_pos[1])
        end = elements.placeholder(*end_pos)
        text = None
        if node.arg("maxCount"):
            max_count = int(node.arg("maxCount"))
            if max_count < 0:
                text = "1..inf"
            else:
                text = f"1..{max_count}"
        self.scene.addItem(elements.backarrow(end_pos, start_pos, text))
        self.scene.addItem(end)
        self.create_connection(parent, end)

        return end, pos

    def __create_decorator__(self, parent, node, pos, color=Qt.white):
        """Create decorator node."""
        if node.ntype == "Repeat":
            return self.__create_repeat__(parent, node, pos, color)
        bg_color = QtGui.QColor(255, 255, 255, 30)
        pen_color = Qt.white
        gap = 6
        rounded = 8
        if node.ntype == "ForceSuccess":
            bg_color = QtGui.QColor(128, 255, 128, 40)
            pen_color = None
            rounded = 9
        if node.ntype == "Inverter":
            gap = 4
            item = elements.create_not()
            item.setPos(pos[0], pos[1])
            self.scene.addItem(item)
            self.create_connection(parent, item)
            pos = (pos[0] + 10, pos[1])
            parent = item
        pos_left = pos
        if node.leafs:
            parent, pos = self.create_node(
                parent,
                node.leafs[0],
                pos,
                color,
            )
        pos_right = pos
        self.scene.addItem(
            elements.create_decorator(
                node,
                pos_left,
                pos_right,
                color=bg_color,
                border=pen_color,
                rounded=rounded,
                gap=gap,
            )
        )
        return parent, pos

    def __setup_end__(self, end_node):
        """Add end node connections."""
        max_x = end_node.pos().x()
        for element in end_node.items:
            ele_x = element.pos().x() + element.width
            if ele_x + self.__delta_x__ > max_x:
                max_x = ele_x + self.__delta_x__
        end_node.setPos(max_x, end_node.pos().y())
        for element in end_node.items:
            self.create_connection(element, end_node)
        self.scene.addItem(end_node)

    def create_connection(self, node_a, node_b, color=Qt.white):
        """Connect two nodes."""
        self.scene.addItem(elements.connect(node_a, node_b, color))

    def create_node(self, parent, node, pos, color=Qt.white):
        """Load node."""
        try:
            node.updated.disconnect(self.__create_view__)
        except RuntimeError:
            # it means is not yet connected...
            pass
        node.updated.connect(self.__create_view__)
        if node.descriptor.category == "Control":
            return self.__create_control__(parent, node, pos, color)
        if node.descriptor.category == "Decorator":
            return self.__create_decorator__(parent, node, pos, color)
        return self.__create_action__(parent, node, pos, color)
