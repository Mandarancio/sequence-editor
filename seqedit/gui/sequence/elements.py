"""Basic UI elements."""
import math

from PySide6 import QtCore, QtGui, QtWidgets


def __cut_text__(text, font, max_width):
    """Make sure text is small enough."""
    metrics = QtGui.QFontMetrics(font)
    text_s = metrics.size(0, text)
    while text_s.width() > max_width and len(text) > 3:
        text = text[:-4] + "..."
        text_s = metrics.size(0, text)
    return text


def create_start():
    """Crete start item."""
    path = QtGui.QPainterPath()
    path.addEllipse(-8, -8, 16, 16)
    return Item(path, fill=QtGui.QColor(66, 132, 66))


def create_end():
    """Crete start item."""
    path = QtGui.QPainterPath()
    path.addRect(-8, -8, 16, 16)
    return Item(path, fill=QtGui.QColor(132, 66, 66))


def create_decorator(
    node,
    pos_0,
    pos_1,
    color=QtGui.QColor(200, 200, 200, 30),
    border=QtCore.Qt.white,
    rounded=8,
    gap=5,
):
    """Create decorator item."""
    # pylint: disable=R0913
    left = pos_0[0] - 5
    bottom = pos_0[1] - (20 + gap)
    right = pos_1[0] + 5
    top = pos_1[1] + (20 + gap)

    width = right - left
    height = top - bottom

    path = QtGui.QPainterPath()
    path.addRoundedRect(
        left,
        bottom,
        width,
        height,
        rounded,
        rounded,
    )
    item = Item(
        path,
        fill=color,
        border=border,
    )
    item.setZValue(-5)
    item.setToolTip(node.ntype)
    # path.addEllipse(0, -5, 10, 10)
    return item  # Item(path, fill=QtGui.QColor(33, 99, 33))


def create_not():
    """Create NOT item."""
    path = QtGui.QPainterPath()
    path.addEllipse(0, -4, 8, 8)
    color = QtGui.QColor(255, 255, 255)
    item = Item(path, fill=color, border=color)
    item.setToolTip("Not")
    return item


def create_label(text):
    """Create label."""
    width = 80
    path = QtGui.QPainterPath()
    font = Item.title_font
    full_text = text
    text = __cut_text__(text, font, width - 5)
    text_h = QtGui.QFontMetrics(font).height()
    path.addText(5, -(text_h / 2 + 2), font, text)
    path.moveTo(0, 0)
    path.lineTo(width, 0)

    item = Item(path, fill=QtCore.Qt.white, border=QtCore.Qt.white)
    item.setToolTip(full_text)
    return item


def create_choice(node):
    """Create decorator item."""
    path = QtGui.QPainterPath()
    path.addPolygon(
        [
            QtCore.QPoint(0, 0),
            QtCore.QPoint(20, 20),
            QtCore.QPoint(40, 0),
            QtCore.QPoint(20, -20),
            QtCore.QPoint(0, 0),
        ]
    )
    item = Item(
        path,
        description=[node.args["var_name"]],
        fill=QtGui.QColor(33, 33, 99),
    )
    item.setToolTip(node.args["var_name"])
    return item


def create_node(node):
    """Create node item."""
    path = QtGui.QPainterPath()
    path.addRoundedRect(0, -20, 80, 40, 4, 4)
    path.moveTo(0, -10)
    path.lineTo(80, -10)
    return Item(path, text=node.ntype, description=node.subtitle)


def placeholder(ps_x=0, ps_y=0):
    """Create placehoder."""
    path = QtGui.QPainterPath()
    item = Item(path)
    item.setPos(ps_x, ps_y)
    return item


def point(pos_x, pos_y):
    """Simply create a point."""
    return QtCore.QPointF(pos_x, pos_y)


def backarrow(start, end, text=None):
    """Create a back arrow."""
    path = QtGui.QPainterPath()
    line_h = 30
    line_r = 8
    line_y = start[1]

    path.moveTo(start[0], start[1])
    path.lineTo(start[0], start[1] - (line_h - line_r))
    path.cubicTo(
        point(start[0], line_y - (line_h - line_r / 2)),
        point(start[0] - line_r / 2, line_y - line_h),
        point(start[0] - line_r, line_y - line_h),
    )
    path.lineTo(end[0] + line_r, line_y - line_h)
    path.cubicTo(
        point(end[0] + line_r / 2, line_y - line_h),
        point(end[0], line_y - (line_h - line_r / 2)),
        point(end[0], line_y - (line_h - line_r)),
    )
    path.lineTo(end[0], end[1] - 5)
    path.addPolygon(
        [
            point(*end),
            point(end[0] - 4, end[1] - 5),
            point(end[0] + 4, end[1] - 5),
            point(*end),
        ]
    )

    path.addEllipse(
        start[0] - 3,
        start[1] - 3,
        6,
        6,
    )
    return Item(path, text=text, fill=None, border=QtCore.Qt.white)


def connect(item_a, item_b, color=QtGui.QColor(255, 255, 255)):
    """Create long connection."""
    pos_a = item_a.center + QtCore.QPointF(item_a.width / 2, 0)
    pos_b = item_b.center - QtCore.QPointF(item_b.width / 2, 0)
    delta_x = 0
    delta_y = 0
    if math.fabs(pos_b.x() - pos_a.x()) >= 10:
        delta_x = 5 if pos_b.x() > pos_a.x() else -5
    if math.fabs(pos_b.y() - pos_a.y()) >= 10:
        delta_y = 5 if pos_b.y() > pos_a.y() else -5

    path = QtGui.QPainterPath()
    path.moveTo(pos_a)
    current = QtCore.QPointF(pos_b.x() - 3 * delta_x, pos_a.y())
    path.lineTo(current)
    ctr0 = current + QtCore.QPointF(delta_x / 2, 0)
    ctr1 = current + QtCore.QPointF(delta_x, delta_y / 2)
    current = current + QtCore.QPointF(delta_x, delta_y)
    path.cubicTo(ctr0, ctr1, current)
    current = QtCore.QPointF(current.x(), pos_b.y() - delta_y)
    path.lineTo(current)
    ctr0 = current + QtCore.QPointF(0, delta_y / 2)
    ctr1 = current + QtCore.QPointF(delta_x / 2, delta_y)
    current = pos_b - QtCore.QPointF(delta_x, 0)
    path.cubicTo(ctr0, ctr1, current)
    path.lineTo(pos_b)
    item = Item(path, fill=None, border=color)
    item.setZValue(-1)
    return item


class Item(QtWidgets.QGraphicsPathItem):
    """Basic UI item."""

    font = QtGui.QFont(
        "Lucida Sans Unicode",
        pointSize=5,
        weight=75,
    )

    title_font = QtGui.QFont(
        "Lucida Sans Unicode",
        pointSize=5,
        weight=50,
    )

    def __init__(
        self,
        path,
        text="",
        description=None,
        fill=QtGui.QColor(33, 33, 33),
        border=QtGui.QColor(200, 200, 200),
    ):
        """Create item."""
        # pylint: disable=R0913
        QtWidgets.QGraphicsPathItem.__init__(self)
        self.setPath(path)
        self.__text_path__ = QtGui.QPainterPath()
        if text:
            self.__create_text__(text)
        if description:
            self.__create_description__(description, has_text=bool(text))
        self.__fill__ = fill
        self.__border__ = border
        self.__connected__ = []

    def __create_text__(self, text):
        """Create text path."""
        path = self.path()
        metrics = QtGui.QFontMetrics(self.font)
        txt_x = path.boundingRect().x()
        txt_y = path.boundingRect().y()
        width = path.boundingRect().width()
        text = __cut_text__(text, self.font, width - 5)
        text_s = metrics.size(0, text)
        self.__text_path__.addText(
            txt_x + width / 2 - text_s.width() / 2,
            txt_y + 5 + text_s.height() / 4,
            self.font,
            text,
        )

    def __create_description__(self, description, has_text):
        """Create description path."""
        path = self.path()
        metrics = QtGui.QFontMetrics(self.title_font)
        width = path.boundingRect().width()
        text_h = metrics.height()
        delta_y = 10 if has_text else 2 * text_h / 3
        y_init = delta_y - ((text_h - 2) * len(description)) / 2
        for i, line in enumerate(description):
            line = __cut_text__(line, self.title_font, width - 5)
            text_w = metrics.size(0, line).width()
            self.__text_path__.addText(
                width / 2 - text_w / 2,
                y_init + i * (text_h - 2),
                self.title_font,
                line,
            )

    def paint(self, painter, _0=None, _1=None):
        """
        Override the default paint method.

        The color and pen style depend on the state of
        the connection.
        """
        if self.__border__:
            pen = QtGui.QPen(self.__border__, 1)
            painter.setPen(pen)
        else:
            painter.setPen(QtCore.Qt.NoPen)
        if self.__fill__:
            painter.setBrush(self.__fill__)
        else:
            painter.setBrush(QtCore.Qt.NoBrush)
        painter.drawPath(self.path())

        painter.setPen(QtCore.Qt.NoPen)
        painter.setBrush(QtCore.Qt.white)
        painter.drawPath(self.__text_path__)

    def add_item(self, item):
        """Add item to the connected items."""
        self.__connected__.append(item)

    @property
    def width(self):
        """Item width."""
        return self.path().boundingRect().width()

    @property
    def center(self):
        """Item center."""
        cen_x = self.path().boundingRect().center().x() + self.pos().x()
        cen_y = self.pos().y()
        return QtCore.QPointF(cen_x, cen_y)

    def move(self, delta):
        """Move node and connected ones."""
        for item in self.__connected__:
            item.setPos(item.pos() + delta)
            item.move(delta)

    @property
    def items(self):
        """Get connected items."""
        return self.__connected__
