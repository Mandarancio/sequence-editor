"""Workspace editor."""
from core.node_info import VariableInfoManager
from core.procedure import Procedure
from gui.edit_widget import edit_variable
from gui.view import EditorView
from PySide6 import QtGui, QtWidgets


class AddVariableDialog(QtWidgets.QDialog):
    """Add variable dialog."""

    def __init__(self, plugins, parent=None):
        """Create dialog UI."""
        QtWidgets.QDialog.__init__(self, parent)
        self.setWindowTitle("Add variable")
        layout = QtWidgets.QFormLayout(self)
        name_edit = QtWidgets.QLineEdit(self)
        type_combobox = QtWidgets.QComboBox(self)
        type_combobox.addItems(VariableInfoManager().variable_types(plugins))

        layout.addRow("Name", name_edit)
        layout.addRow("Type", type_combobox)
        button_box = QtWidgets.QDialogButtonBox(
            QtWidgets.QDialogButtonBox.Ok | QtWidgets.QDialogButtonBox.Cancel,
            self,
        )
        layout.addWidget(button_box)
        button_box.accepted.connect(self.accept)
        button_box.rejected.connect(self.reject)
        self.__type_box__ = type_combobox
        self.__name_edit__ = name_edit

    def var_name(self):
        """Get variable name."""
        return self.__name_edit__.text()

    def var_type(self):
        """Get variable type."""
        return self.__type_box__.currentText()


class VariableList(QtWidgets.QListView):
    """List variables."""

    def __init__(self, tree: Procedure, parent=None):
        """Create view."""
        QtWidgets.QListView.__init__(self, parent)
        model = QtGui.QStandardItemModel(0, 1, self)
        self.setModel(model)
        self.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.__tree__ = tree
        for variable in self.__tree__.workspace:
            self.add_variable(variable)

    def add_variable(self, variable):
        """Add variable to the list."""
        item = QtGui.QStandardItem(f"{variable.name} ({variable.vtype})")
        item.setData(variable)
        self.model().appendRow(item)
        variable.updated.connect(
            lambda: item.setText(f"{variable.name} ({variable.vtype})")
        )
        return item


class WorkspaceEditor(EditorView):
    """Simple workspace editor."""

    def __init__(self, obj, parent=None):
        """Create workspace editor."""
        EditorView.__init__(self, obj, parent)

        split = QtWidgets.QSplitter(self)

        layout = QtWidgets.QHBoxLayout()
        # layout.setMargin(0)

        self.setLayout(layout)
        layout.addWidget(split)
        left = QtWidgets.QWidget(split)
        left_layout = QtWidgets.QVBoxLayout()
        # left_layout.setMargin(1)
        left_layout.addWidget(QtWidgets.QLabel("<b>Variables</b>"))
        list_view = VariableList(self.__procedure__, left)
        left_layout.addWidget(list_view)
        add_btn = QtWidgets.QToolButton()
        rmv_btn = QtWidgets.QToolButton()
        add_btn.setText("+")
        rmv_btn.setText("-")
        left_toolbar = QtWidgets.QWidget(left)
        left_toolbar.setMaximumHeight(30)
        ltool_layout = QtWidgets.QHBoxLayout()
        # ltool_layout.setMargin(0)
        ltool_layout.addWidget(add_btn)
        ltool_layout.addWidget(rmv_btn)
        ltool_layout.addStretch()
        left_layout.addWidget(left_toolbar)
        left_toolbar.setLayout(ltool_layout)
        left.setLayout(left_layout)
        split.addWidget(left)
        self.__list__ = list_view
        right = QtWidgets.QWidget()
        rlayout = QtWidgets.QHBoxLayout(right)
        # rlayout.setMargin(1)
        right.setLayout(rlayout)
        self.__edit_widget__ = None
        self.__right_layout__ = rlayout
        split.addWidget(right)
        split.setSizes([150, 200])

        add_btn.clicked.connect(self.__variable_add__)
        rmv_btn.clicked.connect(self.__variable_remove__)
        list_view.selectionModel().currentChanged.connect(
            self.__selection_update__,
        )

    def __selection_update__(self, current, _):
        """Open editor of the selected variable."""
        item = self.__list__.model().itemFromIndex(current)
        if item is not None and item.data() is not None:
            var = item.data()
            widget = edit_variable(var)
            if self.__edit_widget__ is not None:
                self.__right_layout__.removeWidget(self.__edit_widget__)
                self.__edit_widget__.hide()
                del self.__edit_widget__
            self.__edit_widget__ = widget
            self.__right_layout__.addWidget(widget)

    def __variable_add__(self):
        """Add new variable."""
        dialog = AddVariableDialog(
            [plugin.lib for plugin in self.__procedure__.plugins],
            self,
        )
        res = dialog.exec()
        if res:
            v_name = dialog.var_name()
            v_type = dialog.var_type()
            variable = self.__procedure__.add_variable(v_name, v_type)
            self.__list__.add_variable(variable)

    def __variable_remove__(self):
        """Remove selected variable."""
        index = self.__list__.selectedIndexes()
        if len(index) == 1:
            index = index[0]
            item = self.__list__.model().itemFromIndex(index)
            text = f"Are you sure to remove the variable `{item.data().name}`?"
            res = QtWidgets.QMessageBox.question(
                self,
                "Remove variable",
                text,
            )

            if res == QtWidgets.QMessageBox.StandardButton.Yes:
                self.__procedure__.remove_variable(item.data())
                self.__list__.model().removeRow(index.row())
