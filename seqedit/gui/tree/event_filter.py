"""Class used to manage the user interaction with the node graph view."""

from gui.tree.connection import Connection
from gui.tree.node import Node
from gui.tree.port import Port
from PySide6 import QtCore, QtWidgets

__GRID__ = 15


class EventFilter(QtCore.QObject):
    """Object used to manage all the scene interactions."""

    open_sequence = QtCore.Signal(str, str)  # double click include node.
    open_editor = QtCore.Signal(Node)  # open Node editor
    hide_editor = QtCore.Signal()  # hide node editor
    create_node = QtCore.Signal(Connection)  # create and connect a node.

    def __init__(self, parent):
        """Construct the node editor interaction manager."""
        QtCore.QObject.__init__(self, parent)
        self.connection = None
        self.port = None
        self.scene = None
        self._last_selected = None
        self._drag = None

    def install(self, scene):
        """Install node editor to the node scene."""
        self.scene = scene
        self.scene.installEventFilter(self)

    def item_at(self, position):
        """Retrive item at a cretain position in the scene."""
        items = self.scene.items(
            QtCore.QRectF(position - QtCore.QPointF(1, 1), QtCore.QSizeF(3, 3))
        )

        if items:
            return items[0]
        return None

    def eventFilter(self, watched, event):
        """Fucntion used to filter the ui events."""
        if type(event) == QtWidgets.QWidgetItem:
            return False
        if event.type() == QtCore.QEvent.GraphicsSceneMouseDoubleClick:
            if event.button() == QtCore.Qt.LeftButton:
                item = self.item_at(event.scenePos())
                if isinstance(item, Node):
                    if item.internal.ntype == "Include":
                        seq_name = item.args["path"]
                        path = item.args["file"] if "file" in item.args else ""
                        self.open_sequence.emit(path, seq_name)
        if event.type() == QtCore.QEvent.GraphicsSceneMousePress:
            if event.button() == QtCore.Qt.LeftButton:
                item = self.item_at(event.scenePos())

                if isinstance(item, Port):
                    self.connection = Connection(None)
                    self.scene.addItem(self.connection)
                    # self.connection.start_port = item
                    if item.is_full():
                        other = item.disconnect_last()
                        item = other
                    item.activate()
                    self.port = item
                    self.connection.start_pos = item.scenePos()
                    self.connection.end_pos = event.scenePos()
                    self.connection.update_path()
                    return True

                if isinstance(item, Connection):
                    self.connection = Connection(None)
                    self.connection.start_pos = item.start_pos
                    self.scene.addItem(self.connection)
                    # self.connection.start_port = item.start_port
                    self.port = item.start_port
                    self.connection.end_pos = event.scenePos()
                    self.connection.update_start_and_end_pos()
                    return True

                if isinstance(item, Node):
                    if self._last_selected:
                        # If we clear the scene, we loose the last selection
                        try:
                            self._last_selected.select_connections(False)
                        except RuntimeError:
                            pass

                    item.select_connections(True)
                    self._last_selected = item
                    self._drag = item
                else:
                    try:
                        if self._last_selected:
                            self.hide_editor.emit()
                            self._last_selected.select_connections(False)
                    except RuntimeError:
                        pass

                    self._last_selected = None

            elif event.button() == QtCore.Qt.RightButton:
                # context menu
                pass

        elif event.type() == QtCore.QEvent.KeyPress:
            if event.key() == QtCore.Qt.Key_A:
                """Open menu editor"""
                self.create_node.emit(None)
            if event.key() == QtCore.Qt.Key_Delete:
                for item in self.scene.selectedItems():
                    if isinstance(item, (Connection, Node)):
                        item.delete()

                return True

        elif event.type() == QtCore.QEvent.GraphicsSceneMouseMove:
            if self.connection:
                self.connection.end_pos = event.scenePos()
                self.connection.update_path()
                return True

        elif event.type() == QtCore.QEvent.GraphicsSceneMouseRelease:
            left = event.button() == QtCore.Qt.LeftButton
            if self._last_selected and not self.connection and left:
                self.open_editor.emit(self._last_selected)
            elif self.connection and event.button() == QtCore.Qt.LeftButton:
                if self.connection and not self.port:
                    self.connection.delete()
                    self.connection = None
                    return False
                item = self.item_at(event.scenePos())

                if isinstance(item, Node):
                    port = item.in_port
                    if not self.port.is_output:
                        port = item.out_port
                    item = port
                # connecting a port
                if isinstance(item, Port):
                    if self.port.can_connect_to(item):
                        if not item.is_output:
                            item.clear_connections()

                        self.connection.start_port = self.port

                        self.connection.end_port = item

                        self.port.add_connection(self.connection)
                        item.add_connection(self.connection)

                        self.connection.update_start_and_end_pos()
                        self.connection = None
                    else:
                        self.connection.delete()
                        self.connection = None
                elif self.port.is_output:
                    self.connection.start_port = self.port
                    self.create_node.emit(self.connection)
                    self.connection = None
                if self.connection:
                    self.connection.delete()
                self.port.disactivate()
                self.connection = None
                self.port = None
                return True
            if self._drag is not None:
                self._drag = None

        return QtCore.QObject.eventFilter(self, watched, event)
