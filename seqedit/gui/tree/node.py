"""Node UI method."""
from core.history_manager import State
from core.settings import SettingsManager as Settings
from gui.tree.connection import Connection
from gui.tree.port import Port
from PySide6 import QtCore, QtGui, QtWidgets


class Node(QtWidgets.QGraphicsPathItem, QtCore.QObject):
    """Node class."""

    colors = {
        "Control": QtGui.QColor(120, 120, 30, 200),
        "Decorator": QtGui.QColor(30, 120, 120, 200),
        "Action": QtGui.QColor(20, 20, 20, 200),
    }

    changed = QtCore.Signal()
    deleted = QtCore.Signal()

    def __init__(self, node_object):
        """Create the node.

        TODO: use better node information.
        """
        QtWidgets.QGraphicsPathItem.__init__(self)
        QtCore.QObject.__init__(self)
        self.internal = node_object

        self.setFlag(QtWidgets.QGraphicsPathItem.ItemIsMovable)
        self.setFlag(QtWidgets.QGraphicsPathItem.ItemIsSelectable)

        self.__parent__ = None

        self.__is_root__ = node_object.is_root
        self.__is_leaf__ = node_object.is_leaf
        self._title_text = node_object.ntype
        self._type_text = node_object.subtitle

        self.__width__ = 125  # The Width of the node
        self.__height__ = 75  # the height of the node
        self._ports = []  # A list of ports
        self._in_port = None
        self._out_port = None

        category = node_object.descriptor.category
        if category in self.colors:
            self.node_color = self.colors[category]
        else:
            self.node_color = QtGui.QColor(20, 20, 20, 200)

        self.title_font = QtGui.QFont(
            "Lucida Sans Unicode",
            pointSize=8,
            weight=75,
        )
        self.subtitle_font = QtGui.QFont("Lucida Sans Unicode", pointSize=6)
        self.title_path = QtGui.QPainterPath()  # The path for the title
        self.subtitle_path = QtGui.QPainterPath()  # The path for the type

    def __create_ports__(self):
        """Initialize input and ports."""
        self.add_port()
        if not self.is_leaf:
            self.add_port(is_output=True)

    @property
    def root(self):
        """Parent node."""
        return self.__parent__

    @root.setter
    def root(self, node):
        self.__parent__ = node

    @property
    def is_root(self):
        """Return if the node is root."""
        return self.__is_root__

    @property
    def is_leaf(self):
        """Return if the node is a leaf."""
        return self.__is_leaf__

    @property
    def args(self):
        """Return node arguments."""
        return self.internal.args

    @property
    def title(self):
        """Get title."""
        return self._title_text

    @title.setter
    def title(self, title):
        """Set title."""
        self._title_text = title

    @property
    def type_text(self):
        """Get type text."""
        return self._type_text

    @type_text.setter
    def type_text(self, type_text):
        """Set type text."""
        self._type_text = type_text

    def paint(self, painter, option=None, widget=None):
        """Paint node widget."""
        if self.isSelected():
            painter.setPen(QtGui.QPen(QtGui.QColor(241, 175, 0), 2))
            painter.setBrush(self.node_color)
        else:
            painter.setPen(QtGui.QPen(QtGui.QColor(200, 200, 200), 1))
            painter.setBrush(self.node_color)

        painter.drawPath(self.path())
        painter.setPen(QtCore.Qt.NoPen)
        painter.setBrush(QtCore.Qt.white)

        painter.drawPath(self.title_path)
        painter.drawPath(self.subtitle_path)

    def add_port(self, is_output=False):
        """Add additional output/input port."""
        port = Port(self)
        port.set_is_output(is_output)

        self._ports.append(port)
        if is_output:
            self._out_port = port
        else:
            self._in_port = port

    def __build_title__(self):
        """Build title path."""
        self.title_path = QtGui.QPainterPath()  # reset
        title = self._title_text
        title_fm = QtGui.QFontMetrics(self.title_font)
        title_s = title_fm.size(0, title)
        title_h = title_s.height()
        title_w = title_s.width()
        while title_w > self.__width__ - 10 and len(title) > 4:
            title = title[:-4] + "..."
            title_s = title_fm.size(0, title)
            title_w = title_s.width()

        # Draw the title
        self.title_path.addText(
            -title_w / 2,
            (-self.__height__ / 2) + title_h + 7,
            self.title_font,
            title,
        )

    def __build_subtitle__(self):
        """Build subtitle path."""
        self.subtitle_path = QtGui.QPainterPath()  # The path for the type
        title_h = QtGui.QFontMetrics(self.title_font).height()
        type_fm = QtGui.QFontMetrics(self.subtitle_font)
        type_h = type_fm.height()

        for i, type_text in enumerate(self._type_text):
            type_s = type_fm.size(0, type_text)
            while type_s.width() > self.__width__ - 10 and len(type_text) > 4:
                type_text = type_text[:-4] + "..."
                type_s = type_fm.size(0, type_text)

            # Draw the type
            self.subtitle_path.addText(
                -type_s.width() / 2,
                (-self.__height__ / 2) + 7 + title_h + (i + 1) * type_h,
                self.subtitle_font,
                type_text,
            )

    def __build_node__(self):
        """Pass for the moment."""
        path = QtGui.QPainterPath()  # The main path

        # Draw the background rectangle
        path.addRoundedRect(
            -self.__width__ / 2,
            -self.__height__ / 2,
            self.__width__,
            self.__height__,
            4,
            4,
        )

        return path

    def build(self):
        """Build the node user interface."""
        self.__create_ports__()
        self.internal.updated.connect(self.__updated__)
        self.internal.updated.connect(self.changed.emit)

        # The fonts what will be used

        path = self.__build_node__()
        self.__build_title__()
        self.__build_subtitle__()
        for port in self._ports:
            port_x = (
                self.__width__ / 2
                if Settings().tree_layout == Settings.horizontal
                else 0
            )
            port_y = (
                self.__height__ / 2
                if Settings().tree_layout == Settings.vertical
                else 0
            )
            if port.is_output:
                port.setPos(port_x, port_y)
            else:
                port.setPos(-port_x, -port_y)
        self.setPath(path)

    def select_connections(self, state):
        """Select connections."""
        for port in self._ports:
            for connection in port.connections:
                connection.do_highlight = state
                connection.update_path()

    def connections(self):
        """Return all connections."""
        res = []
        for port in self._ports:
            res += port.connections.copy()
        return res

    def contextMenuEvent(self, event):
        """Open menu."""
        menu = QtWidgets.QMenu(self)
        pos = event.pos()

        # actions
        delete_node = QtWidgets.QAction("Delete Node")
        edit_node = QtWidgets.QAction("Edit Node")
        menu.addAction(delete_node)

        action = menu.exec_(self.mapToGlobal(pos))

        if action == delete_node:
            item_name = self.selectedItems()[0].text()

            if item_name not in ["And", "Not", "Input", "Output"]:
                print(f"delete node: {item_name}")
            else:
                print("Cannot delete default nodes")

        elif action == edit_node:
            print("editing node")

            # confirm to open in the editor replacing what is existing

    def delete(self):
        """Delete the connection.

        Remove any found connections ports by calling :any:
        `Port.remove_connection`.  After connections have been removed set the
        stored :any:`Port` to None. Lastly call :any:
        `QGraphicsScene.removeItem` on the scene to remove this widget.
        """
        to_delete = []

        for port in self._ports:
            for connection in port.connections:
                to_delete.append(connection)

        for connection in to_delete:
            connection.delete()

        self.deleted.emit()
        self.scene().removeItem(self)

    def connected(self, _):
        """Add new node to connected list."""
        self.__update_leafs__()

    def disconnected(self, other):
        """Remove node from connected list."""
        if other:
            self.internal.remove_leaf(other.internal)

    def connect_to(self, other):
        """
        Connect this node to another node.

        Arguments:
        ---------
            other: other node to be connected.

        """
        if not self.is_connected(other):
            a_port = self.out_port
            b_port = other.in_port

            other.root = self

            connection = Connection(None)
            a_port.add_connection(connection, setup=True)
            b_port.add_connection(connection, setup=True)
            self.scene().addItem(connection)
            connection.start_port = a_port
            connection.end_port = b_port
            connection.update_start_and_end_pos()

    def is_connected(self, other):
        """Check if this node is already connected to another node."""
        if self.in_port and other in [
            cnn.start_node for cnn in self.in_port.connections
        ]:
            return True
        if self.out_port and other in [
            cnn.end_node for cnn in self.out_port.connections
        ]:
            return True
        return False

    @property
    def out_port(self):
        """Get output port."""
        return self._out_port

    @property
    def in_port(self):
        """Get input port."""
        return self._in_port

    def mouseMoveEvent(self, event: QtWidgets.QGraphicsSceneMouseEvent):
        """Move node."""
        if event.scenePos().x() >= 0 and event.scenePos().y() >= 0:
            super().mouseMoveEvent(event)
            modifier = event.modifiers() == QtCore.Qt.ControlModifier
            left_btn = event.buttons() & QtCore.Qt.LeftButton
            # If modifier is pressed only move current node.
            if left_btn and modifier != Settings().move_leafs:
                delta = event.scenePos() - event.lastScenePos()
                self.__move_connected__(delta)

    def mouseReleaseEvent(self, event: QtWidgets.QGraphicsSceneMouseEvent):
        """Mouse release event."""
        super().mouseReleaseEvent(event)
        left_btn = event.button() == QtCore.Qt.MouseButton.LeftButton
        if left_btn and self.root:
            self.root.__update_leafs__()
        elif left_btn:
            self.changed.emit()

    def __update_leafs__(self):
        """Update leaf list position."""
        self.internal.leafs = [node.internal for node in self.ordered_leafs]

    def move(self, delta):
        """Move node."""
        self.setPos(self.pos() + delta)
        self.__move_connected__(delta)

    def __move_connected__(self, delta):
        """Move all connected nodes (if any)."""
        for leaf in self.leafs:
            leaf.move(delta)

    def __has_input__(self):
        """Check if input port exists."""
        return self.in_port is not None

    def remove_input(self):
        """Remove input port."""
        if self.in_port:
            port = self.in_port
            connections = port.connections
            self.scene().removeItem(port)
            self._ports.remove(port)
            self._in_port = None
            for connection in connections:
                connection.delete()

    def __updated__(self):
        """Refresh node."""
        self._type_text = self.internal.subtitle
        self.__build_subtitle__()
        self.update()

    @property
    def leafs(self):
        """Get leaf nodes."""
        leafs = []
        if self.out_port:
            connections = self.out_port.connections
            leafs = [
                connection.other_node(self)
                for connection in connections
                if connection.other_node(self) is not None
            ]
        return leafs

    @property
    def ordered_leafs(self):
        """Get leaf ordered from left to right."""
        leafs = self.leafs
        return sorted(leafs, key=lambda node: node.pos().x())

    def anchestor(self, other):
        """Check if this node is anchestor of other node."""
        for leaf in self.leafs:
            if leaf == other or leaf.anchestor(other):
                return True
        return False

    def __repr__(self):
        """Print info."""
        return f"{self.internal.ntype} [{self.pos().x()}, {self.pos().y()}]"

    def state(self):
        """Create current node state."""
        in_state = self.in_port.state() if self.in_port else None
        out_state = self.out_port.state() if self.out_port else None

        return State(
            self,
            pos=self.pos(),
            internal=self.internal.state(),
            in_port=in_state,
            out_port=out_state,
        )

    def set_state(self, state):
        """Set node state."""
        self.setPos(state["pos"])
        if state["in_port"]:
            state["in_port"].apply()
        if state["out_port"]:
            state["out_port"].apply()
        state["internal"].apply()
