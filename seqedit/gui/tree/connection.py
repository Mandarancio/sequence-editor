"""Node connection ui."""
from core.history_manager import State
from core.settings import SettingsManager
from PySide6 import QtCore, QtGui, QtWidgets


class Connection(QtWidgets.QGraphicsPathItem):
    """Item representing the connection between two nodes."""

    def __init__(self, parent):
        """Construct connection."""
        QtWidgets.QGraphicsPathItem.__init__(self, parent)

        self.setFlag(QtWidgets.QGraphicsPathItem.ItemIsSelectable)

        self.setPen(QtGui.QPen(QtGui.QColor(200, 200, 200), 2))
        self.setBrush(QtCore.Qt.NoBrush)
        self.setZValue(-1)

        self._start_port = None
        self._end_port = None

        self.start_pos = QtCore.QPointF()
        self.end_pos = QtCore.QPointF()

        self._do_highlight = False

    def delete(self):
        """Delete connection."""
        for port in (self._start_port, self._end_port):
            if port:
                port.remove_connection(self)

                # port.connection = None
            port = None

        self.scene().removeItem(self)

    @property
    def start_port(self):
        """Start (node) port."""
        return self._start_port

    @property
    def end_port(self):
        """End (node) port."""
        return self._end_port

    @start_port.setter
    def start_port(self, port):
        self._start_port = port
        self._start_port.connection = self

    @end_port.setter
    def end_port(self, port):
        self._end_port = port
        self._end_port.connection = self

    @property
    def do_highlight(self):
        """Get do_highlight field."""
        return self._do_highlight

    @do_highlight.setter
    def do_highlight(self, value):
        self._do_highlight = bool(value)

    def nodes(self):
        """Return the connected nodes."""
        return (self._start_port.node, self._end_port.node)

    def other_node(self, node):
        """Get other node."""
        if self._start_port and self._start_port.node != node:
            return self._start_port.node
        if self._end_port and self._end_port.node != node:
            return self._end_port.node
        return None

    def highlight(self):
        """Highlight selection."""
        self._do_highlight = True

    def update_start_and_end_pos(self):
        """
        Update the ends of the connection.

        Get the start and end ports and use them to set the start
        and end positions.
        """
        if self.start_port and not self.start_port.is_output:
            temp = self.end_port
            self._end_port = self.start_port
            self._start_port = temp

        if self._start_port:
            self.start_pos = self._start_port.scenePos()

        # if we are pulling off an exiting connection we skip code below
        if self._end_port:
            self.end_pos = self._end_port.scenePos()

        self.update_path()

    @property
    def end_node(self):
        """Get end node."""
        if self._end_port:
            return self._end_port.node
        return None

    @property
    def start_node(self):
        """Get start node."""
        if self._start_port:
            return self._start_port.node
        return None

    def update_path(self):
        """Draw a smooth cubic curve from the start to end ports."""
        path = QtGui.QPainterPath()
        path.moveTo(self.start_pos)

        delta_x = self.end_pos.x() - self.start_pos.x()
        delta_y = self.end_pos.y() - self.start_pos.y()
        if SettingsManager().tree_layout == SettingsManager.horizontal:
            ctr1 = QtCore.QPointF(
                self.start_pos.x() + delta_x * 0.5,
                self.start_pos.y(),
            )
            ctr2 = QtCore.QPointF(
                self.start_pos.x() + delta_x * 0.5,
                self.start_pos.y() + delta_y,
            )
        else:
            ctr1 = QtCore.QPointF(
                self.start_pos.x(),
                self.start_pos.y() + delta_y * 0.5,
            )
            ctr2 = QtCore.QPointF(
                self.start_pos.x() + delta_x,
                self.start_pos.y() + delta_y * 0.5,
            )

        path.cubicTo(ctr1, ctr2, self.end_pos)

        self.setPath(path)

    def paint(self, painter, option=None, widget=None):
        """
        Override the default paint method.

        The color and pen style depend on the state of
        the connection.
        """
        if self.isSelected() or self._do_highlight:
            pen = QtGui.QPen(QtGui.QColor(255, 102, 0), 3)
        else:
            pen = QtGui.QPen(QtGui.QColor(0, 128, 255), 2)
        if self._end_port is None or self._start_port is None:
            pen.setColor(QtGui.QColor(200, 200, 200))
            pen.setStyle(QtCore.Qt.DashLine)
        painter.setPen(pen)
        painter.drawPath(self.path())

    def state(self):
        """Get current state."""
        return State(
            self,
            start_port=self.start_port,
            end_port=self.end_port,
        )

    def set_state(self, state):
        """Set state."""
        self._start_port = state["start_port"]
        self._end_port = state["end_port"]
        self.start_node.connect_to(self.end_node)
        self.update_path()
