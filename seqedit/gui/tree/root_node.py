"""Sequence virtual root node."""
from gui.tree.node import Node
from PySide6 import QtGui


class RootNode(Node):
    """Root Node."""

    def __init__(self, node_object):
        """Create Root node."""
        Node.__init__(self, node_object)
        self.__width__ = 70
        self.__height__ = 70
        self.node_color = QtGui.QColor(30, 30, 90, 200)

    def __build_node__(self):
        """Create root path."""
        path = QtGui.QPainterPath()  # The main path

        path.addEllipse(
            -self.__width__ / 2,
            -self.__height__ / 2,
            self.__width__,
            self.__height__,
        )
        return path

    def __build_title__(self):
        """Build root title."""
        title = self._title_text
        title_fm = QtGui.QFontMetrics(self.title_font)
        title_w = title_fm.size(0, title).width()
        self.title_path.addText(
            -title_w / 2,
            0,
            self.title_font,
            title,
        )

    def __build_subtitle__(self):
        """Nothing to do."""

    def delete(self):
        """Delete is not possible for root node."""

    def __create_ports__(self):
        """Create root ports."""
        self.add_port(is_output=True)
