"""Adaptive menu."""
from core.node_info import categories, node_descriptors
from PySide6 import QtWidgets


class Menu(QtWidgets.QMenu):
    """Smart menu to add new nodes."""

    def __init__(self, procedure, parent=None):
        """Initialize menu."""
        QtWidgets.QMenu.__init__(self, parent)
        self.setTitle("Add Node")
        self.__line_edit__ = QtWidgets.QLineEdit()
        self.__line_edit__.setPlaceholderText("Search...")
        widget = QtWidgets.QWidget()
        layout = QtWidgets.QHBoxLayout(widget)
        # layout.setMargin(2)
        layout.addWidget(self.__line_edit__)
        action = QtWidgets.QWidgetAction(self)
        action.setDefaultWidget(widget)
        self.__line_action__ = action
        self.__menus__ = []
        self.__descriptors__ = node_descriptors(
            [plugin.lib for plugin in procedure.plugins]
        )
        self.__default_menu__()
        # connect
        self.__line_edit__.textChanged.connect(self.__search__)
        self.__line_edit__.returnPressed.connect(self.__return_pressed__)

    def __find_nodes__(self, text):
        """Find all nodes that match the text."""
        nodes = []
        for cat in categories():
            flag = text.lower() in cat.lower()
            for node in self.__descriptors__[cat]:
                if flag or text.lower() in node.lower():
                    nodes.append(node)
        return nodes

    def __default_menu__(self):
        """Create the default menu."""
        self.addAction(self.__line_action__)
        for category in categories():
            title = category.capitalize()
            menu = self.addMenu(title)
            for node in self.__descriptors__[category]:
                menu.addAction(node)
            self.__menus__.append(menu)
        self.__line_edit__.setFocus()

    def __clear_menu__(self):
        """Clear menu."""
        for action in self.actions():
            self.removeAction(action)

    def __search_menu__(self, nodes):
        """Search action."""
        self.__clear_menu__()
        self.addAction(self.__line_action__)
        self.__line_edit__.setFocus()
        if len(nodes) > 0:
            for node in nodes:
                self.addAction(node)
            self.setActiveAction(self.actions()[1])

    def __search__(self, text):
        """Search string."""
        if text:
            nodes = self.__find_nodes__(text)
            self.__search_menu__(nodes)
        else:
            self.__clear_menu__()
            self.__default_menu__()

    def __return_pressed__(self):
        """Return key pressed."""
        action = self.activeAction()
        self.triggered.emit(action)
