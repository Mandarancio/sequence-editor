"""Widget to manage the node view."""
from core.history_manager import HistoryManager, State
from core.node_info import VROOT_TYPE
from core.procedure import make_node
from core.settings import SettingsManager as Settings
from gui.edit_widget import edit
from gui.tree.event_filter import EventFilter
from gui.tree.menu import Menu
from gui.tree.node import Node
from gui.tree.root_node import RootNode
from gui.view import GraphEditorView
from PySide6 import QtGui


def create_node(node):
    """Create a node."""
    if node.ntype == VROOT_TYPE:
        node_gui = RootNode(node)
    else:
        node_gui = Node(node)
    node_gui.build()
    return node_gui


class TreeEditor(GraphEditorView):
    """Main widget that contains node scene."""

    def __init__(self, sequence, parent=None):
        """Create node widget."""
        GraphEditorView.__init__(self, sequence, EventFilter(None), parent)

        self.__event_filter__.open_sequence.connect(self.open_element.emit)
        self.__event_filter__.create_node.connect(self.__create_and_connect__)
        self.__current_state__ = None
        self.__side_widget__ = None
        self.__setting_state__ = False

    def __open_editor__(self, node: Node):
        """Show node editor in the sidebar."""
        if Settings().center_on_focused:
            self.view.move_to(node)

        if self.__side_widget__ is None:
            self.__splitter__.setSizes([1000, 300])

        if self.__side_widget__ is not None:
            self.__side_layout__.removeWidget(self.__side_widget__)
            self.__side_widget__.deleteLater()
            del self.__side_widget__
            self.__side_widget__ = None
        self.__side_widget__ = edit(node.internal)
        self.__side_layout__.addWidget(self.__side_widget__)
        self.__sidebar__.show()
        self.update()

    def __context_menu__(self, pos):
        """Open add node menu."""
        menu = Menu(self.__procedure__, self)
        action = menu.exec_(pos)
        if action is not None and action.text():
            pos = self.view.mapFromGlobal(pos)
            self.__add_node__(
                pos.x(),
                pos.y() + 32,
                action.text(),
            )

    def create_node(self, node, pos=None):
        """Create new node."""
        node = create_node(node)

        self.scene.addItem(node)
        if pos is None:
            pos = self.view.mapFromGlobal(QtGui.QCursor.pos())
            node.setPos(self.view.mapToScene(pos))
        else:
            node.setPos(*pos)
        node.setVisible(True)
        node.setZValue(5)

        node.deleted.connect(self.__update_history__)
        node.changed.connect(self.__update_history__)
        return node

    def __add_node__(self, pos_x, pos_y, text):
        """Add node to scene."""
        node_object = make_node(text, self.__procedure__)
        if node_object:
            node = create_node(node_object)
            self.scene.addItem(node)
            node.setPos(self.view.mapToScene(pos_x, pos_y))
            node.setVisible(True)
            node.deleted.connect(self.__update_history__)
            node.changed.connect(self.__update_history__)
            if Settings().center_new_nodes:
                self.view.move_to(node)
                # self.view.centerOn(node)
            self.__update_history__()
            return node
        return None

    def __create_and_connect__(self, connection):
        """Create a node and connect it to its parent."""
        pos = QtGui.QCursor.pos()
        menu = Menu(self.__procedure__, self)

        action = menu.exec_(pos)
        pos = self.view.mapFromGlobal(pos)
        if action is not None and action.text():
            node = self.__add_node__(pos.x(), pos.y() + 32, action.text())
            if node is not None and connection is not None:
                connection.end_port = node.in_port
                connection.start_port.add_connection(connection)
                node.in_port.add_connection(connection)
                connection.update_start_and_end_pos()
                self.__update_history__()
                return True
        if connection:
            connection.delete()
        return False

    def __load_sequence__(self):
        """Load a sequence."""
        root, _ = self.__load_node__(self.__sequence__, matrix={})
        self.view.centerOn(root)
        self.__current_state__ = self.state()

    def __load_normal_node__(
        self,
        node,
        matrix,
        center,
        level,
        delta0,
    ):
        """Load node in wide mode."""
        if node is None:
            return None, 0

        delta = 0
        if level in matrix:
            delta = matrix[level]
            if delta > delta0:
                matrix[level] += 1
            else:
                delta = delta0
                matrix[level] = delta0 + 1
        else:
            matrix[level] = delta0 + 1
            delta = delta0
        nodes = []
        if node.leafs:
            lcount = len(node.leafs)
            ldelta0 = delta - lcount // 2
            ldelta0 = 0 if ldelta0 < 0 else ldelta0
            for snode in node.leafs:
                node_x, _ = self.__load_node__(
                    snode,
                    matrix,
                    center=center,
                    level=level + 1,
                    delta0=ldelta0,
                )
                nodes.append(node_x)
        if level > 0:
            level_y = (level - 1) * 175 + 130
        else:
            level_y = 0
        if Settings().tree_layout == Settings.horizontal:
            pos_x = center[0] + level_y
            pos_y = center[1] + 90 * delta
        else:
            pos_y = center[1] + level_y
            pos_x = center[0] + 150 * delta

        node_x = self.create_node(node, pos=(pos_x, pos_y))
        for leaf in nodes:
            node_x.connect_to(leaf)
        return node_x, delta

    def __load_wide_node__(
        self,
        node,
        matrix,
        center,
        level,
        delta0,
    ):
        """Load node in wide mode."""
        if node is None:
            return None, 0

        delta = 0
        if level in matrix:
            delta = matrix[level]
            if delta > delta0:
                matrix[level] += 1
            else:
                delta = delta0
                matrix[level] = delta0 + 1
        else:
            matrix[level] = delta0 + 1
            delta = delta0
        nodes = []
        if node.leafs:
            delta_min = 10000
            lcount = len(node.leafs)
            ldelta0 = delta - lcount // 2
            ldelta0 = 0 if ldelta0 < 0 else ldelta0
            for snode in node.leafs:
                node_x, ldelta = self.__load_node__(
                    snode,
                    matrix,
                    center=center,
                    level=level + 1,
                    delta0=ldelta0,
                )
                nodes.append(node_x)
                if ldelta < delta_min:
                    delta_min = ldelta
            delta_max = matrix[level + 1]
            matrix[level + 1] += 1
            delta = (delta_min + delta_max) // 2
            matrix[level] = delta + 1
        if level > 0:
            level_y = (level - 1) * 175 + 130
        else:
            level_y = 0
        if Settings().tree_layout == Settings.horizontal:
            pos_x = center[0] + level_y
            pos_y = center[1] + 90 * delta
        else:
            pos_y = center[1] + level_y
            pos_x = center[0] + 150 * delta

        node_x = self.create_node(node, pos=(pos_x, pos_y))
        for leaf in nodes:
            node_x.connect_to(leaf)
        return node_x, delta

    def __load_node__(
        self,
        node,
        matrix,
        center=(150, 150),
        level=0,
        delta0=0,
    ):
        """Load node and place the node ui."""
        if Settings().wide_mode:
            return self.__load_wide_node__(
                node,
                matrix,
                center,
                level,
                delta0,
            )
        return self.__load_normal_node__(
            node,
            matrix,
            center,
            level,
            delta0,
        )

    def __update_history__(self):
        """Update history."""
        if not self.__setting_state__:
            HistoryManager().update(self.__current_state__)
            self.__current_state__ = self.state()

    def state(self):
        """Get current state."""
        return State(
            self,
            viewport=self.view.viewport(),
            nodes=[node.state() for node in self.nodes()],
        )

    def set_state(self, state):
        """Set editor state."""
        self.__setting_state__ = True
        nodes = self.nodes()
        s_nodes = [sst.source for sst in state["nodes"]]
        for node in nodes:
            if node not in s_nodes:
                self.scene.removeItem(node)
        for n_state in state["nodes"]:
            if n_state.source not in nodes:
                self.scene.addItem(n_state.source)
            n_state.apply()
        self.view.setViewport(state["viewport"])
        self.__current_state__ = state
        self.__setting_state__ = False

    def nodes(self):
        """Get all nodes."""
        nodes = []
        for item in self.scene.items():
            if issubclass(type(item), Node):
                nodes.append(item)
        return nodes
