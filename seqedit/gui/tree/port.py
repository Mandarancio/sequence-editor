"""Node port UI."""
from core.history_manager import State
from PySide6 import QtGui, QtWidgets


class Port(QtWidgets.QGraphicsPathItem):
    """Class representing a Node port."""

    def __init__(self, parent):
        """Initialize the port of a node."""
        QtWidgets.QGraphicsPathItem.__init__(self, parent)

        self.radius_ = 7.5
        self.margin = 2

        path = QtGui.QPainterPath()
        path.addEllipse(
            -self.radius_, -self.radius_, 2 * self.radius_, 2 * self.radius_
        )
        self.setPath(path)

        self.setFlag(QtWidgets.QGraphicsPathItem.ItemSendsScenePositionChanges)
        self.font = QtGui.QFont()
        self.font_metrics = QtGui.QFontMetrics(self.font)

        self.port_text_height = self.font_metrics.height()

        self._is_output = False
        self.margin = 2

        self.m_node = parent
        self._max_outputs = 0
        self.connections = []

        self.__active__ = False
        self.text_path = QtGui.QPainterPath()

    def activate(self):
        """Activate port."""
        self.__active__ = True

    def disactivate(self):
        """Disactivate port."""
        self.__active__ = False

    @property
    def is_active(self):
        """Check if it is active."""
        return self.__active__

    def set_is_output(self, is_output):
        """Define port as output port."""
        self._is_output = is_output
        if self.m_node.internal.descriptor:
            self._max_outputs = self.m_node.internal.descriptor.outputs
        else:
            print("Warning, unknown node: ", self.m_node.internal)
            self._max_outputs = -1

    def set_node(self, node):
        """Set parent node."""
        self.m_node = node

    @property
    def is_output(self):
        """Check if port is output."""
        return self._is_output

    @property
    def node(self):
        """Get parent node."""
        return self.m_node

    def paint(self, painter, option=None, widget=None):
        """Paint function."""
        if self.is_active:
            painter.setBrush(QtGui.QColor(128, 255, 128))
            painter.setPen(QtGui.QPen(QtGui.QColor(200, 200, 200), 2))
        elif self.is_full():
            painter.setBrush(QtGui.QColor(128, 0, 255))
            painter.setPen(QtGui.QPen(QtGui.QColor(200, 200, 200), 2))
        elif self.is_connected:
            painter.setBrush(QtGui.QColor(0, 128, 255))
            painter.setPen(QtGui.QPen(QtGui.QColor(200, 200, 200), 2))
        else:
            painter.setBrush(QtGui.QColor(66, 66, 66))
            painter.setPen(QtGui.QPen(QtGui.QColor(100, 100, 100), 2))
        painter.drawPath(self.path())

    def disconnect_last(self):
        """Disconnect last connection and return other node."""
        if self.is_connected:
            connection = self.connections[-1]
            other = (
                connection.start_port
                if connection.start_port != self
                else connection.end_port
            )
            other.remove_connection(connection)
            connection.delete()
            # self.connections = self.connections[:-1]
            return other
        return self

    def clear_connections(self):
        """Remove all connections."""
        for connection in self.connections:
            connection.delete()
        self.connections.clear()

    def is_full(self):
        """Check if is full."""
        return (
            self._is_output
            and self._max_outputs > 0
            and len(self.connections) >= self._max_outputs
        ) or (not self._is_output and self.is_connected)

    def can_connect_to(self, port):
        """Check if this port can connect to an other."""
        if self._is_output:
            in_port = port
            out_port = self
        else:
            in_port = self
            out_port = port

        in_node = in_port.node
        out_node = out_port.node

        if in_node == out_node or in_node.anchestor(out_node):
            return False

        if port.is_full():
            port.disconnect_last()
            return True
        if not port or port is None or port.node == self.node:
            return False

        if self._is_output == port.is_output:
            return False

        return True

    @property
    def is_connected(self):
        """Check if the port is connected."""
        if self.connections:
            return True
        return False

    def itemChange(self, change, value):
        """Update port."""
        if change == QtWidgets.QGraphicsItem.ItemScenePositionHasChanged:
            for connection in self.connections:
                connection.update_start_and_end_pos()

        return value

    def remove_connection(self, connection, _=False):
        """Remove one specific connection."""
        if connection in self.connections:
            self.connections.remove(connection)
            if self._is_output:
                other = connection.other_node(self.node)
                self.node.disconnected(other)

    def add_connection(self, connection, setup=False):
        """Add connection to port."""
        if connection not in self.connections:
            self.connections.append(connection)
            if self._is_output:
                other = connection.other_node(self.node)
                if not setup:
                    self.node.connected(other)
            elif not self._is_output and connection.start_port:
                other = connection.other_node(self.node)
                self.node.root = other

    def state(self):
        """Get current state."""
        connections = []
        for connection in self.connections:
            connections.append((connection.start_node, connection.end_node))
        return State(
            self,
            connections=connections,
        )

    def set_state(self, state):
        """Set current state."""
        connections = {}
        for connection in self.connections:
            key = (connection.start_node, connection.end_node)
            connections[key] = connection
        for key in connections:
            if key not in state["connections"]:
                connection = connections[key]
                if connection in self.scene().items():
                    self.scene().removeItem(connection)
                self.connections.remove(connection)
        if self._is_output:
            for _, end in state["connections"]:
                if not self.node.is_connected(end):
                    self.node.connect_to(end)
