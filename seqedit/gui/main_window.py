"""Main entry point for Graph Editor."""
import logging
import os
import sys

import core.parser as seqparser
from core.history_manager import HistoryManager
from core.settings import SettingsManager as Settings
from gui.file_manager import SeqManager
from gui.palette import dark_palette
from PySide6 import QtCore, QtWidgets
from PySide6.QtUiTools import QUiLoader

# LOGGING LEVEL
logging.basicConfig(level=logging.DEBUG)


def __close_event__():
    """Close app."""
    Settings().save()


class AppManager:
    """Manage the QApplication and main window."""

    def __init__(self, path=None, tree=None):
        """Initialize the application with or without opening a file."""
        self.__app__ = QtWidgets.QApplication([])
        # qtmodern.styles.dark(self.__app__)
        loader = QUiLoader()
        py_dir = os.path.dirname(os.path.abspath(__file__))
        self.__ui_path__ = os.path.join(py_dir, "resources")
        self.__loader__ = loader
        ui_path = os.path.join(self.__ui_path__, "window.ui")
        self.__window__ = loader.load(ui_path, None)
        self.__tab_manager__ = SeqManager(parent=self.__window__)
        if path is not None and tree is not None:
            self.__tab_manager__.load_procedure(path, tree)
        self.__window__.setCentralWidget(self.__tab_manager__)
        self.__window__.actionOpen.triggered.connect(self.__open__)
        self.__window__.actionSaveAs.triggered.connect(
            self.__tab_manager__.save_as,
        )
        self.__window__.actionSave.triggered.connect(
            self.__tab_manager__.save,
        )
        self.__window__.actionNew.triggered.connect(
            self.__tab_manager__.new_procedure,
        )
        self.__window__.actionUndo.triggered.connect(
            HistoryManager().undo,
        )
        self.__window__.actionRedo.triggered.connect(
            HistoryManager().redo,
        )
        self.__window__.actionAbout.triggered.connect(self.__about__)

        self.__window__.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
        self.__window__.destroyed.connect(__close_event__)

        self.__app__.setStyle("Fusion")
        if Settings().dark_mode:
            self.__app__.setPalette(dark_palette())

    def __about__(self):
        """Show about dialog."""
        path = os.path.join(self.__ui_path__, "about.ui")
        dialog = self.__loader__.load(path, self.__window__)
        dialog.exec_()

    @property
    def app(self):
        """Get QT App object."""
        return self.__app__

    @property
    def window(self):
        """Get main window object."""
        return self.__window__

    def __open__(self):
        """Open file dialog."""
        path = QtWidgets.QFileDialog.getOpenFileName(
            caption="Open Procedure file",
            filter="xml files (*.xml)",
            parent=self.__window__,
        )[0]
        if path:
            path = os.path.relpath(path)
            tree = seqparser.parse(path)
            self.__tab_manager__.load_procedure(path, tree)


if __name__ == "__main__":
    app_manager = AppManager()
    # app.setPalette(palette)
    app_manager.window.show()
    app_manager.app.exec_()
    sys.exit()
