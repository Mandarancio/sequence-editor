"""Section view."""
from core.settings import SettingsManager
from PySide6.QtWidgets import QTabWidget


def dynimport(name):
    """Import dynamically a module."""
    mod_path = ".".join(name.split(".")[:-1])
    cls_name = name.split(".")[-1]
    mod = __import__(mod_path, fromlist=[cls_name])
    return getattr(mod, cls_name)


class Section(QTabWidget):
    """View that only show the workspace and plugins tabs."""

    def __init__(self, obj, file_manager, parent=None):
        """Create tabs and initialize view."""
        QTabWidget.__init__(self, parent)
        self.setMinimumWidth(600)
        self.setTabPosition(QTabWidget.TabPosition.South)
        plugins = SettingsManager().plugins(type(obj).__name__)
        for label in plugins:
            class_path = plugins[label]
            view_class = dynimport(class_path)
            view = view_class(obj, parent=self)
            view.initialize_view()
            view.open_element.connect(file_manager.open_from_path)
            self.addTab(view, label)
