"""Basic constants."""
from PySide6 import QtGui

BACKGROUND_COLOR = QtGui.QColor(38, 38, 38)

GRID_PEN_S = QtGui.QPen(QtGui.QColor(52, 52, 52, 255), 0.5)
GRID_PEN_L = QtGui.QPen(QtGui.QColor(22, 22, 22, 255), 1.0)

GRID_SIZE_FINE = 15
GRID_SIZE_COURSE = 150

MOUSE_WHEEL_ZOOM_RATE = 0.0015
