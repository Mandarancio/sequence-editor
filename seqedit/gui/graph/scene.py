"""Basic Node scene."""
from PySide6 import QtWidgets


class NodeScene(QtWidgets.QGraphicsScene):
    """Scene containing all nodes."""

    def dragEnterEvent(self, e):
        """Accept drag event."""
        e.acceptProposedAction()

    def dropEvent(self, e):
        """Accept drop event."""
        # find item at these coordinates
        item = self.itemAt(e.scenePos())
        if item.setAcceptDrops:
            # pass on event to item at the coordinates
            try:
                item.dropEvent(e)
            except RuntimeError:
                pass

    def dragMoveEvent(self, e):
        """Drag event."""
        e.acceptProposedAction()
