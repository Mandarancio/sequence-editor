"""Graphics view that manage the scene rendering."""
from gui.graph import scene_style as style
from PySide6 import QtCore, QtGui, QtOpenGL, QtWidgets
from PySide6.QtWidgets import QGraphicsView


class View(QGraphicsView):
    """Main view port of the Graph."""

    context_menu = QtCore.Signal(QtCore.QPoint)
    item_context_menu = QtCore.Signal(object, QtCore.QPoint)

    def __init__(self, parent):
        """Create Sequence View."""
        QtWidgets.QGraphicsView.__init__(self, parent)
        self.setRenderHint(QtGui.QPainter.Antialiasing)
        # gl_format = QtOpenGL.QGLFormat(QtOpenGL.QGL.SampleBuffers)
        # gl_format.setSamples(10)
        # gl_widget = QtOpenGL.QGLWidget(gl_format)
        self.__current_scale__ = 1

        self.__pan__ = {
            "state": False,
            "x": 0,
            "y": 0,
        }

        self.__num_scheduled_scalings__ = 0
        self.__last_mouse_pos__ = QtCore.QPoint()
        self.__anim__ = None

        # self.setViewport(gl_widget)

        self.setTransformationAnchor(QtWidgets.QGraphicsView.AnchorUnderMouse)
        self.setResizeAnchor(QtWidgets.QGraphicsView.AnchorUnderMouse)
        self.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.setFrameShape(QtWidgets.QFrame.NoFrame)

    def center(self):
        """Get center view point."""
        scale = self.viewportTransform().m11()
        trans = QtCore.QPointF(
            self.viewportTransform().dx(),
            self.viewportTransform().dy(),
        )
        center = QtCore.QPointF(self.viewport().rect().center())
        return (center - trans) / scale

    def move_to(self, item):
        """Animate centerOn method."""
        final_pos = item.pos() + item.boundingRect().center()
        anim = QtCore.QTimeLine(350, self)
        anim.setUpdateInterval(20)
        anim.valueChanged.connect(
            lambda val: self.centerOn(
                self.center() + (final_pos - self.center()) * val,
            ),
        )
        anim.start()

    def wheelEvent(self, event):
        """Manage wheel event."""
        # sometimes you can triger the wheen when panning so we disable
        # when panning
        if self.__pan__["state"]:
            return

        num_degrees = (event.angleDelta() / 8.0).y()
        num_steps = num_degrees / 5.0
        self.__num_scheduled_scalings__ += num_steps

        # If the user moved the wheel another direction, we
        # reset previously scheduled
        if self.__num_scheduled_scalings__ * num_steps < 0:
            self.__num_scheduled_scalings__ = num_steps

        self.__anim__ = QtCore.QTimeLine(350, self)
        self.__anim__.setUpdateInterval(20)

        self.__anim__.valueChanged.connect(self.__scaling_time__)
        self.__anim__.finished.connect(self.__anim_finished__)
        self.__anim__.start()

    def __scaling_time__(self, _):
        """Scaling factor."""
        factor = 1.0 + self.__num_scheduled_scalings__ / 300.0
        self.__current_scale__ *= factor
        self.scale(factor, factor)

    def __anim_finished__(self):
        """Finish scaling animation."""
        if self.__num_scheduled_scalings__ > 0:
            self.__num_scheduled_scalings__ -= 1
        else:
            self.__num_scheduled_scalings__ += 1

    def drawBackground(self, painter, rect):
        """Draw checkboard background."""
        painter.fillRect(rect, style.BACKGROUND_COLOR)

        left = int(rect.left()) - (int(rect.left()) % style.GRID_SIZE_FINE)
        top = int(rect.top()) - (int(rect.top()) % style.GRID_SIZE_FINE)

        # Draw horizontal fine lines
        grid_lines = []
        painter.setPen(style.GRID_PEN_S)
        y = float(top)
        while y < float(rect.bottom()):
            grid_lines.append(QtCore.QLineF(rect.left(), y, rect.right(), y))
            y += style.GRID_SIZE_FINE

        # Draw vertical fine lines
        x = float(left)
        while x < float(rect.right()):
            grid_lines.append(QtCore.QLineF(x, rect.top(), x, rect.bottom()))
            x += style.GRID_SIZE_FINE
        painter.drawLines(grid_lines)

        # Draw thick grid
        left = int(rect.left()) - (int(rect.left()) % style.GRID_SIZE_COURSE)
        top = int(rect.top()) - (int(rect.top()) % style.GRID_SIZE_COURSE)

        # Draw vertical thick lines
        grid_lines = []
        painter.setPen(style.GRID_PEN_L)
        x = left
        while x < rect.right():
            grid_lines.append(QtCore.QLineF(x, rect.top(), x, rect.bottom()))
            x += style.GRID_SIZE_COURSE

        # Draw horizontal thick lines
        y = top
        while y < rect.bottom():
            grid_lines.append(QtCore.QLineF(rect.left(), y, rect.right(), y))
            y += style.GRID_SIZE_COURSE
        painter.drawLines(grid_lines)

        return QGraphicsView.drawBackground(self, painter, rect)

    def contextMenuEvent(self, event):
        """
        Open context menu.

        TODO: implement proper menus.
        """
        cursor = QtGui.QCursor()
        # origin = self.mapFromGlobal(cursor.pos())
        pos = self.mapFromGlobal(cursor.pos())
        item = self.itemAt(event.pos())

        if not item:
            self.context_menu.emit(self.mapToGlobal(pos))
        else:
            self.item_context_menu.emit(item, self.mapToGlobal(pos))

    def dragEnterEvent(self, e):
        """Handle drag enter event."""
        if e.mimeData().hasFormat("text/plain"):
            e.accept()
        else:
            e.ignore()

    def dropEvent(self, event):
        """Handle drop event."""
        event.ignore()

    def mousePressEvent(self, event):
        """Handle mouse press event: start drag."""
        if event.button() == QtCore.Qt.MiddleButton:
            self.__pan__["state"] = True
            self.__pan__["x"] = event.x()
            self.__pan__["y"] = event.y()
            self.setCursor(QtCore.Qt.ClosedHandCursor)

        return QGraphicsView.mousePressEvent(self, event)

    def mouseReleaseEvent(self, event):
        """Handle mouse release event: stop drag."""
        if event.button() == QtCore.Qt.MiddleButton:
            self.__pan__["state"] = False
            self.setCursor(QtCore.Qt.ArrowCursor)

        return QGraphicsView.mouseReleaseEvent(self, event)

    def mouseMoveEvent(self, event):
        """Handle mouse movement event: drag if needed."""
        if self.__pan__["state"]:
            delta_x = event.x() - self.__pan__["x"]
            delta_y = event.y() - self.__pan__["y"]
            self.horizontalScrollBar().setValue(
                self.horizontalScrollBar().value() - delta_x
            )

            self.verticalScrollBar().setValue(
                self.verticalScrollBar().value() - delta_y
            )
            self.__pan__["x"] = event.x()
            self.__pan__["y"] = event.y()

        return QGraphicsView.mouseMoveEvent(self, event)
