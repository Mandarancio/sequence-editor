"""Simple singleton implementation."""


class SingletonMeta(type):
    """Simple singleton metaclass."""

    __instances__ = {}

    def __call__(cls, *args, **kwargs):
        """Ensure only one instance."""
        if cls not in cls.__instances__:
            instance = super().__call__(*args, **kwargs)
            cls.__instances__[cls] = instance
        return cls.__instances__[cls]
