"""Manage app settings."""
import os
from pathlib import Path
import yaml
from core.singleton import SingletonMeta


class SettingsManager(metaclass=SingletonMeta):
    """Setting manager class."""

    __local_dir__ = [".local", "share", "seqedit"]
    __conf_file__ = "config.yml"

    __K_CENTER_NEW_NODES__ = "center_new_nodes"
    __K_CENTER_ON_FOCUSED__ = "center_on_focused"
    __K_DARK_MODE__ = "dark_mode"
    __K_MOVE_LEAFS__ = "move_leafs"
    __K_TREE_LAYOUT__ = "tree_layout"
    __K_WIDE_MODE__ = "wide_mode"

    __K_VIEWS__ = "views_plugins"

    horizontal = "horizontal"
    vertical = "vertical"

    def __init__(self):
        """Create setting manager."""
        self.__settings__ = {}
        path = self.settings_path()
        self.__path__ = os.path.join(path, self.__conf_file__)
        self.__settings__ = None
        self.__ui_views__ = None

        if os.path.isfile(self.__path__):
            self.__load_settings__()
        else:
            self.default_settings()
        if self.__K_VIEWS__ not in self.__settings__:
            self.__load_default_plugins__()

        self.save()

    def __load_settings__(self):
        """Load settings from file."""
        with open(self.__path__, "r") as doc:
            self.__settings__ = yaml.safe_load(doc)

    def settings_path(self):
        """Get setting path."""
        path = os.path.join(str(Path.home()), *self.__local_dir__)
        if not os.path.isdir(path):
            os.makedirs(path)
        return path

    def default_settings(self):
        """Use default settings."""
        self.__settings__ = {
            self.__K_CENTER_NEW_NODES__: False,
            self.__K_CENTER_ON_FOCUSED__: False,
            self.__K_DARK_MODE__: True,
            self.__K_MOVE_LEAFS__: True,
            self.__K_TREE_LAYOUT__: self.vertical,
            self.__K_WIDE_MODE__: True,
        }
        self.__load_default_plugins__()

    def __load_default_plugins__(self):
        """Load default plugins."""
        yml_path = os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            "resources",
            "plugins.yml",
        )
        view_plugins = {}
        with open(yml_path) as yml_file:
            view_plugins = yaml.safe_load(yml_file)
        self.__settings__[self.__K_VIEWS__] = view_plugins

    def plugins(self, objtype):
        """Retrive map of plugins available for the given type."""
        if objtype in self.__settings__[self.__K_VIEWS__]:
            return self.__settings__[self.__K_VIEWS__][objtype]
        return []

    def save(self):
        """Save current settings to file."""
        with open(self.__path__, "w") as doc:
            doc.write(
                yaml.safe_dump(
                    self.__settings__,
                    sort_keys=False,
                )
            )

    def __get_setting__(self, key):
        """Retrive setting value."""
        if key in self.__settings__:
            return self.__settings__[key]
        return None

    @property
    def center_on_focused(self):
        """Center on focused node property."""
        return self.__get_setting__(self.__K_CENTER_ON_FOCUSED__)

    @property
    def center_new_nodes(self):
        """Get `center new nodes` setting value."""
        return self.__get_setting__(self.__K_CENTER_NEW_NODES__)

    @property
    def move_leafs(self):
        """Get `move leaf default` setting value."""
        return self.__get_setting__(self.__K_MOVE_LEAFS__)

    @property
    def tree_layout(self):
        """Tree layout horientation."""
        return self.__get_setting__(self.__K_TREE_LAYOUT__)

    @property
    def dark_mode(self):
        """Dark mode property."""
        return self.__get_setting__(self.__K_DARK_MODE__)

    @property
    def wide_mode(self):
        """Wide mode property."""
        return self.__get_setting__(self.__K_WIDE_MODE__)


if __name__ == "__main__":
    obj_a = SettingsManager()
    obj_b = SettingsManager()
    print(id(obj_a) == id(obj_b))
    print(obj_a.__settings__)

    obj_a.save()
    print("exit")
