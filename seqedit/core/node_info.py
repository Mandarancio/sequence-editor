"""Handle the node informations."""
import os
from shutil import copyfile

import yaml

from core.singleton import SingletonMeta
from core.settings import SettingsManager

VROOT_TYPE = "root"
__KEY_CORE__ = "_"


def __load_yaml__():
    # pylint: disable=E1136
    local_path = SettingsManager().settings_path()
    yml_path = os.path.join(local_path, "nodes.yml")
    if not os.path.isfile(yml_path):
        def_path = os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            "resources",
            "nodes.yml",
        )
        copyfile(def_path, yml_path)
    obj = None
    with open(yml_path) as file:
        obj = yaml.safe_load(file.read())
        if not obj:
            return {}
        res = {}
        for name, plugin in obj.items():
            res[name] = {}
            for cat_name, category in plugin.items():
                if cat_name not in res:
                    res[name][cat_name] = {}
                for ntype, arg in category.items():
                    res[name][cat_name][ntype] = NodeTypeInfo(
                        ntype,
                        cat_name,
                        arg,
                    )
        return res
    return obj


def __load_var_yaml__():
    # pylint: disable=E1136
    local_path = SettingsManager().settings_path()
    yml_path = os.path.join(local_path, "workspace.yml")
    if not os.path.isfile(yml_path):
        def_path = os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            "resources",
            "workspace.yml",
        )
        copyfile(def_path, yml_path)
    obj = None
    with open(yml_path) as file:
        obj = yaml.safe_load(file.read())
        if not obj:
            return {}
        res = {}
        for name, plugin in obj.items():
            res[name] = {}
            for vtype, var in plugin.items():
                res[name][vtype] = {}
                for arg_name, arg in var.items():
                    res[name][vtype][arg_name] = ArgType(arg_name, arg)
        return res
    return None


def node_info(node):
    """Get Node information."""
    return NodeInfoManager().node_info(node)


def variable_info(variable):
    """Get Variable information."""
    return VariableInfoManager().variable_info(variable)


def categories():
    """Get category list."""
    return NodeInfoManager().categories


def node_descriptors(plugins):
    """Return map of descriptors."""
    return NodeInfoManager().descriptors(plugins)


class ArgType:
    """Handle arg type."""

    def __init__(self, name, data):
        """Initialize Argument type from yaml struct."""
        self.__arg_name__ = name
        self.__dtype__ = "none"
        self.__is_optional__ = False
        self.__range__ = {}
        self.__choices__ = None
        if isinstance(data, str):
            self.__dtype__ = data
        elif isinstance(data, list):
            self.__dtype__ = "choice"
            self.__choices__ = data
        elif isinstance(data, dict) and "type" in data:
            if isinstance(data["type"], list):
                self.__dtype__ = "choice"
                self.__choices__ = data["type"]
            elif isinstance(data["type"], str):
                self.__dtype__ = data["type"]
            else:
                self.__dtype__ = "UNKNOWN"
            if "min" in data:
                self.__range__["min"] = data["min"]
            if "max" in data:
                self.__range__["max"] = data["max"]
            if "mandatory" in data:
                self.__is_optional__ = not data["mandatory"]

    @property
    def name(self):
        """Get argument name."""
        return self.__arg_name__

    @property
    def type_name(self):
        """Return type name."""
        return self.__dtype__

    @property
    def is_optional(self):
        """Check if argument is optional."""
        return self.__is_optional__

    @property
    def has_choices(self):
        """Check if it has choices."""
        return self.__choices__ is not None

    @property
    def choices(self):
        """Get list of possible choices."""
        if self.has_choices:
            return self.__choices__
        return []

    @property
    def has_range(self):
        """Check if a range is defined."""
        return "max" in self.__range__ or "min" in self.__range__

    @property
    def max(self):
        """Get maximum possible value."""
        if "max" in self.__range__:
            return self.__range__["max"]
        return 999999999

    @property
    def min(self):
        """Get minimum possible value."""
        if "min" in self.__range__:
            return self.__range__["min"]
        return -999999999


def __parse_args__(args):
    res = {}
    for arg in args:
        res[arg] = ArgType(arg, args[arg])
    return res


class NodeTypeInfo:
    """Handle Yaml node information in a more proper way."""

    def __init__(self, name: str, category: str, info: dict):
        """Initialize node info class."""
        self.__type_name__ = name
        self.__category__ = category
        self.__outputs__ = 0 if "outputs" not in info else info["outputs"]
        self.__args__ = {
            "name": ArgType(
                "name",
                {
                    "type": "str",
                    "mandatory": False,
                },
            )
        }
        if "args" in info:
            self.__args__.update(__parse_args__(info["args"]))

    @property
    def name(self):
        """Get node type names."""
        return self.__type_name__

    @property
    def outputs(self):
        """Get max number of outpts (-1 = inf)."""
        return self.__outputs__

    @property
    def arguments(self):
        """Get dict of arguments."""
        return self.__args__

    @property
    def category(self):
        """Get node category."""
        return self.__category__

    def make_args(self):
        """Create a dictionary with default arguments."""
        args = {}
        for name, arg in self.__args__.items():
            if not arg.is_optional:
                args[name] = None
        return args


class NodeInfoManager(metaclass=SingletonMeta):
    """Manage node informations."""

    def __init__(self):
        """Consturctor that initialize the nodes information map."""
        self.__nodes__ = __load_yaml__()
        self.__vroot__ = NodeTypeInfo(
            "root",
            None,
            {
                "outputs": 1,
                "args": {
                    "isRoot": "bool",
                    "name": "str",
                },
            },
        )

    def node_info(self, node):
        """Retrive node information."""
        name = node if isinstance(node, str) else node.ntype
        if name == VROOT_TYPE:
            return self.__vroot__
        for plugin in self.__nodes__.values():
            for category in plugin.values():
                if name in category:
                    return category[name]
        return None

    def node_max_outputs(self, node):
        """Get maximum number of oputus."""
        info = self.node_info(node)
        if info is not None and "outputs" in info:
            return info["outputs"]
        return 0

    @property
    def categories(self):
        """Get list of categories."""
        return self.__nodes__[__KEY_CORE__].keys()

    def descriptors(self, plugins):
        """Get all descriptors."""
        descs = self.__nodes__[__KEY_CORE__].copy()
        for plugin in plugins:
            if plugin in self.__nodes__:
                for catname, category in self.__nodes__[plugin].items():
                    descs[catname].update(category)
        return descs


class VariableInfoManager(metaclass=SingletonMeta):
    """Manage variable descriptors."""

    def __init__(self):
        """Initialize variable info manager."""
        self.__variables__ = __load_var_yaml__()

    def variable_info(self, variable):
        """Get variable descriptor."""
        vtype = variable if isinstance(variable, str) else variable.vtype
        for plugin in self.__variables__.values():
            if vtype in plugin:
                return plugin[vtype]
        return None

    def variable_types(self, plugins):
        """Get list of all variable types."""
        varlist = list(self.__variables__[__KEY_CORE__].keys())
        for plugin in plugins:
            if plugin in self.__variables__:
                varlist += list(self.__variables__[plugin].keys())
        return varlist

    def make_args(self, vtype):
        """Create variable default arguments."""
        args = {}
        desc = self.variable_info(vtype)
        if desc:
            for arg_name in desc:
                arg = desc[arg_name]
                if not arg.is_optional:
                    args[arg_name] = None
        return args
