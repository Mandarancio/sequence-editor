"""Simple state history manager."""

from core.singleton import SingletonMeta


class HistoryManager(metaclass=SingletonMeta):
    """State history manager."""

    def __init__(self):
        """Initialize the history manager."""
        self.__history__ = []
        self.__future__ = []

    def update(self, state):
        """Update the history with a new state."""
        self.__history__.append(state)
        self.__future__.clear()

    def undo(self):
        """Undo action."""
        if self.__history__:
            state = self.__history__.pop()
            new_state = state.source.state()
            state.apply()
            self.__future__.append(new_state)

    def redo(self):
        """Redo action."""
        if self.__future__:
            state = self.__future__.pop()
            new_state = state.source.state()
            state.apply()
            self.__history__.append(new_state)


class State:
    """Simple state."""

    def __init__(self, source, **kwargs):
        """Initialize state values."""
        self.__source__ = source
        self.__state__ = kwargs

    @property
    def source(self):
        """State source object."""
        return self.__source__

    @property
    def values(self):
        """Get internal state values."""
        return self.__state__

    def __getitem__(self, key):
        """Get state informations."""
        if key in self.__state__:
            return self.__state__[key]
        return None

    def apply(self):
        """Apply state to source."""
        self.__source__.set_state(self)
