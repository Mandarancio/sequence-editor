#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Sequencer XML parser."""
from lxml import etree

from core.procedure import Procedure, Node, Root, Variable, Plugin

__NS__ = {"ns": "http://codac.iter.org/sup/sequencer"}


class SeqParserError(Exception):
    """Simple Exception for Parsing Errors."""


def __parse_workspace__(root):
    workspace = root.xpath("//ns:Workspace", namespaces=__NS__)
    if workspace is not None and len(workspace) > 0:
        variables = []
        for var in workspace[0]:
            variables.append(
                Variable(
                    var.tag.split("}")[1],
                    var.attrib["name"],
                    args=var.attrib,
                )
            )
        return variables
    return None


def __parse_plugins__(root):
    plugins = root.xpath("//ns:Plugin", namespaces=__NS__)
    res = []
    for plugin in plugins:
        res.append(Plugin(plugin.text))
    return res


def __parse_node__(elem):
    ntype = elem.tag.split("}")[1]
    leafs = []
    kwargs = elem.attrib
    for leaf in elem:
        leafs.append(__parse_node__(leaf))
    node = Node(ntype, leafs, **kwargs)
    return node


def __parse_sequences__(root):
    seqs = root.xpath(
        "/ns:Procedure/ns:*[name(.) != 'Plugin']",
        namespaces=__NS__,
    )
    res = {}
    untitled = 0
    for seq in seqs:
        tag = seq.tag.split("}")[1]
        if tag != "Workspace":
            name = None
            if "name" in seq.attrib:
                name = seq.attrib["name"]
            if name is None:
                if untitled:
                    name = f"untitled {untitled}"
                else:
                    name = "untitled"
                untitled += 1
                seq.set("name", name)
            node = Root(__parse_node__(seq))
            res[name] = node
    return res


def parse(path):
    """
    Parse XML Sequencer file.

    Arguments
    ---------
     - path : path of the XML file to parse.

    Returns
    -------
    Sequence tree structure.

    """
    with open(path) as seqfile:
        xml = etree.parse(seqfile)
        root = xml.getroot()

        if root.tag == "{" + __NS__["ns"] + "}Procedure":
            workspace = __parse_workspace__(root)
            plugins = __parse_plugins__(root)
            sequences = __parse_sequences__(root)
            procedure = Procedure(
                root.attrib["name"],
                workspace=workspace,
                plugins=plugins,
                sequences=sequences,
            )
            procedure.path = path
            return procedure
        raise SeqParserError("XML is not a sequence.")
