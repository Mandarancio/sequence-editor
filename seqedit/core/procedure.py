"""Procedure and sequences representation."""
from core.history_manager import State
from core.node_info import VROOT_TYPE, VariableInfoManager, node_info
from lxml import etree
from PySide6 import QtCore


def make_node(name, procedure):
    """Create a node from the name."""
    descriptor = node_info(name)
    if descriptor is None:
        return None
    node = Node(name, [], **descriptor.make_args())
    node.procedure = procedure
    return node


class Procedure:
    """Class that used to represent a sequencer file."""

    def __init__(
        self,
        name,
        workspace,
        sequences,
        plugins,
    ):
        """
        Procedure constructor.

        Arguments:
        ---------
            name: procedure name
            workspace: workspace definition (default: None)
            sequences: dictionary of all defined sequences (default: {})
            plugins: list of used plugins (default: [])

        """
        self.__name__ = name
        self.__workspace__ = workspace
        self.__sequences__ = sequences
        self.__plugins__ = plugins
        self.__path__ = None

        for seq_name in sequences:
            sequences[seq_name].procedure = self

    def xml(self):
        """Get XML representation of the procedure."""
        seqns = "http://codac.iter.org/sup/sequencer"
        nsmap = {None: seqns}
        root = etree.Element(
            "Procedure",
            name=self.name,
            version="1.0",
            nsmap=nsmap,
        )
        for plugin in self.plugins:
            child = etree.Element("Plugin")
            child.text = plugin.lib
            root.append(child)
        for name in self.sequences:
            root.append(self.sequences[name].xml())
        workspace = etree.Element("Workspace")
        for var in self.__workspace__:
            workspace.append(var.xml())
        root.append(workspace)
        return root

    @property
    def path(self):
        """Procedure path."""
        return self.__path__

    @path.setter
    def path(self, value):
        self.__path__ = value

    @property
    def name(self):
        """Return the name of the procedure."""
        return self.__name__

    @name.setter
    def name(self, new_name):
        self.__name__ = new_name

    @property
    def workspace(self):
        """Return the workspace definition."""
        return self.__workspace__

    @property
    def plugins(self):
        """Retrun the list of plugins."""
        return self.__plugins__

    def add_plugin(self, plugin):
        """Add a plugin to the procedure."""
        if isinstance(plugin, str):
            plugin = Plugin(plugin)
        self.__plugins__.append(plugin)

    def remove_plugin(self, plugin):
        """Remove a plugin from the procedure."""
        if plugin in self.__plugins__:
            self.__plugins__.remove(plugin)

    def add_sequence(self, sequence):
        """Add a sequence to the list of sequences."""
        name = sequence.name
        if not name:
            name = "untitled"
        i = 0
        base = name
        while name in self.__sequences__:
            i += 1
            name = f"{base} {i}"
        sequence.update_field("name", name)
        self.__sequences__[name] = sequence

    def remove_sequence(self, sequence):
        """Search and remove sequence."""
        name = None
        for key in self.__sequences__:
            if self.__sequences__[key] == sequence:
                name = key
        if name:
            self.__sequences__.pop(name)

    @property
    def sequences(self):
        """Retrurn the defined sequences."""
        return self.__sequences__

    def remove_variable(self, variable):
        """Remove a variable from the workspace."""
        self.__workspace__.remove(variable)

    def add_variable(self, var_name, var_type):
        """Create a new variable."""
        args = VariableInfoManager().make_args(var_type)
        if args:
            if "name" in args:
                args["name"] = var_name
            var = Variable(
                var_type,
                var_name,
                args,
            )
            self.__workspace__.append(var)
            return var
        return None

    def __repr__(self):
        """Return short representation of the tree."""
        return f"{self.name}"


class Node(QtCore.QObject):
    """Represent a node of the behaviour tree structure."""

    updated = QtCore.Signal()

    def __init__(self, ntype, leafs, **kwargs):
        """
        Node constructor.

        Arguments:
        ---------
            ntype: node type
            leafs: node leafs
            kwargs: other arguments

        """
        QtCore.QObject.__init__(self)
        self.__ntype__ = ntype
        self.__leafs__ = leafs
        self.__args__ = dict(kwargs)
        self.__info__ = node_info(self)
        self.__procedure__ = None

    def update_field(self, field, value):
        """Update the value of a field."""
        if field not in self.__args__ or value != self.__args__[field]:
            self.__args__[field] = value
            self.updated.emit()

    def remove_field(self, field):
        """Remove a field."""
        if field in self.__args__:
            del self.__args__[field]
            self.updated.emit()

    def xml(self):
        """Get XML representation of the node."""
        txt_args = {}
        for arg in self.__args__:
            txt_args[arg] = str(self.__args__[arg])
        node = etree.Element(self.ntype, **txt_args)

        for leaf in self.__leafs__:
            node.append(leaf.xml())
        return node

    @property
    def path(self):
        """Procedure file path."""
        if self.__procedure__:
            return self.__procedure__.path
        return None

    @property
    def procedure(self):
        """Parent procedure."""
        return self.__procedure__

    @procedure.setter
    def procedure(self, value):
        self.__procedure__ = value
        for leaf in self.__leafs__:
            leaf.procedure = value

    @property
    def name(self):
        """Get the name of the node."""
        if "name" in self.__args__:
            return self.__args__["name"]
        return ""

    @property
    def descriptor(self):
        """Get the node descriptor."""
        return self.__info__

    @property
    def leafs(self):
        """Return list of the leafs of the current node."""
        return self.__leafs__

    @leafs.setter
    def leafs(self, leafs):
        if isinstance(leafs, list):
            self.__leafs__ = leafs
            self.updated.emit()

    @property
    def is_leaf(self):
        """Return if the current node is a leaf or not."""
        if self.__info__ is None:
            return True
        return self.__info__.outputs == 0

    @property
    def is_root(self):
        """Check if current node is root."""
        if "isRoot" not in self.__args__:
            return False
        arg = self.__args__["isRoot"]
        if isinstance(arg, bool):
            return arg
        if isinstance(arg, str):
            return arg.lower() == "true"
        return bool(self.__args__["isRoot"])

    @property
    def args(self):
        """Return additional arguments."""
        return self.__args__

    def arg(self, key):
        """Get safely an argument."""
        if key in self.args:
            return self.args[key]
        return None

    @property
    def ntype(self):
        """Return the type of node."""
        return self.__ntype__

    def remove_leaf(self, leaf):
        """Remove a leaf from the leafs list."""
        if leaf and leaf in self.__leafs__:
            self.__leafs__.remove(leaf)
            self.updated.emit()

    def pprint(self, head=""):
        """Pretty print tree."""
        for i, leaf in enumerate(self.leafs):
            if i < len(self.leafs) - 1:
                leaf.pprint(head=head + " |")
            else:
                leaf.pprint(head=" " * len(head) + " |")

    def __repr__(self):
        """Get short string representation of the node."""
        return self.__ntype__

    @property
    def subtitle(self):
        """Get subtitle string."""
        ntype = self.__ntype__
        text = []
        if self.name:
            text.append(self.name)
        if ntype == "Include":
            text.append(self.arg("path"))
        elif ntype == "Repeat":
            text.append(f'max: {self.arg("maxCount")}')
        elif ntype == "Equals":
            text += [self.arg("lhs"), "=", self.arg("rhs")]
        elif ntype == "Copy":
            text += [self.arg("input"), "to", self.arg("output")]
        elif ntype == "Wait":
            text += [f'{self.arg("timeout")} s']
        elif ntype == "MathExpressionNode":
            text += [self.arg("expression")]
        elif ntype == "LogTrace":
            text += [self.arg("message"), f"${self.arg('input')}"]
        return text

    def state(self):
        """Get current node state."""
        return State(
            self,
            args=self.__args__.copy(),
            leafs=self.__leafs__.copy(),
        )

    def set_state(self, state):
        """Set current state."""
        self.__args__ = state["args"]
        self.__leafs__ = state["leafs"]
        self.updated.emit()


class Root(Node):
    """Root virtual node."""

    def __init__(self, leaf):
        """Create root node."""
        Node.__init__(
            self,
            VROOT_TYPE,
            [leaf],
            name=leaf.name,
            isRoot=leaf.is_root,
        )

    def xml(self):
        """Return leaf xml."""
        if self.__leafs__:
            elem = self.__leafs__[0].xml()
            elem.set("isRoot", str(self.is_root))
            elem.set("name", str(self.name))
            return elem
        return None


class Plugin:
    """Sequence plugin."""

    def __init__(self, lib):
        """
        Plugin costructor.

        Arguments:
        ---------
            lib: plugin library.

        """
        self.__lib__ = lib

    @property
    def lib(self):
        """Return the plugin library."""
        return self.__lib__

    def __repr__(self):
        """Return short plugin str representation."""
        return f"Pluging: {self.__lib__}"


class Variable(QtCore.QObject):
    """Sequence Variable."""

    updated = QtCore.Signal()

    def __init__(self, vtype, name, args=None):
        """Variable constructor.

        Arguments:
        ---------
            vtype: type of the variable.
            name: name of the variable.
            args: optional variable arguments.

        """
        QtCore.QObject.__init__(self)
        self.__vtype__ = vtype
        self.__args__ = args
        self.__args__["name"] = name

    def xml(self):
        """Get XML representaion of the variable."""
        txt_args = {}
        for arg in self.__args__:
            txt_args[arg] = str(self.__args__[arg])
        return etree.Element(self.vtype, **txt_args)

    @property
    def vtype(self):
        """Return type of variable."""
        return self.__vtype__

    @property
    def name(self):
        """Retrun name of variable."""
        return self.__args__["name"]

    @property
    def args(self):
        """Return optionals node arguments."""
        return self.__args__

    def update_field(self, field, value):
        """Update a field of the variable."""
        if field in self.__args__ and self.__args__[field] == value:
            return
        self.__args__[field] = value
        self.updated.emit()

    def __repr__(self):
        """Retrun variable short str representation."""
        return f"{self.name} ({self.vtype})"
