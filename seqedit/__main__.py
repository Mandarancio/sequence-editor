#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""SeqEdit main script."""
import sys
from argparse import ArgumentParser

from core.parser import parse
from gui.main_window import AppManager


def __args__():
    """Parse command line arguments."""
    parser = ArgumentParser()
    parser.add_argument(
        "-o", "--open", dest="file_path", help="sequence to open in the editor"
    )
    parser.add_argument(
        "-vm",
        "--view_mode",
        dest="view_mode",
        help="open in view only",
        action="store_true",
    )
    parser.set_defaults(file_path=None, view_mode=False)
    return parser.parse_args()


def __run__(args):
    """Run application."""
    app_manager = AppManager(**args)

    app_manager.window.show()
    app_manager.app.exec()
    sys.exit()


def main():
    """Start the SeqEdit application."""
    args = __args__()
    kwargs = {}
    if args.file_path is not None:
        kwargs["tree"] = parse(args.file_path)
        kwargs["path"] = args.file_path
    __run__(kwargs)


if __name__ == "__main__":
    main()
