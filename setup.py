#!/usr/bin/env python3
"""
Simple setup script for the Sequence-Editor app.
"""

from setuptools import setup
from setuptools.command.install import install as _install


class Install(_install):
    """Installer class."""

    def run(self):
        """Installs modules."""
        _install.do_egg_install(self)


setup(
    name="sequence-editor",
    cmdclass={"install": Install},
    description="A tool to view and edit sequence for the Sequencer xml.",
    author="Martino Ferrari",
    author_email="martinogiordano.ferrari@iter.org",
    version="0.1.0",
    url="https://gitlab.com/Mandarancio/sequence-editor",
    license="MIT",
    platforms=["any"],
    packages=["seqedit"],
    install_requires=["pyyaml", "pyside6", "lxml"],
    setup_requires=[],
    # Add test suite
    # Add entry point
    entry_points={
        "gui_scripts": ["seqedit = seqedit.main:main"],
    },
)
