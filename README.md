# Sequence editor

The editor is around 90% done, the developed functionalities are:

- Create, open and save sequencer porjects
- Procedure edit:
  - Manage the basic procedure information
  - Manage the workspace variables: the editor has a variable descriptor and show the appropriate editor
  - Manage the plugins
  - Add/Remove sequences
- Sequence:
  - View/edit as a tree graph
    - The nodes can be reorganized
    - New nodes can be added with simple user interaction
    - The property of the nodes can be edited: the editor has a node descriptor and show the appropiate editor
    - Undo/Redo functionality
  - View as sequence
    - Show the tree as an actual sequence by using the control and decorator node to modify the sequence flow

## Technicalities

Use `python` as language and `PySide6` (Qt6) for the GUI, other libraries used are `lxml` for the xml parsing and exporting and `pyyaml` for the configuration files.

### Descriptors

The descriptors (both for nodes and variables) are stored as `yaml` files in the user home and are easily extensible.

The nodes descriptor file is organized as such:

```yaml
PLUGIN_NAME: # e.g. libsequencer-ca.so
	CATEGORY: # Control/Descriptor/Action
		NODETYPE: # e.g. Wait
			output: NUMBER_OF_OUTPUT # -1 for infinite, 0 by default
			args:
				ARG_NAME_0: TYPE # if only the type is specified the argument is mandatory
				ARG_NAME_1:
					type: TYPE # str/param/int/float/bool/file
					mandatory: true/false
					# additional info such max/min

```

The variables descriptor file is similar:

```yaml
PLUGIN_NAME:
	VARIABLE_TYPE:
		ARG_NAME_0: TYPE
		ARG_NAME_1:
			type: TYPE
			mandatory: true/false
			# additional info
```

### Settings

Few aspect of the UI and some action are user configurable (by editing a `yaml` file ) such as graph orientation, sub tree drag and more.

The views are extendibles and it is possible to add and change the views by changing the setting file.

## Quality

For the moment the project does not implement any _unit test_, however the project is statically analyzed by `pylint`, `flak8` and `pydocstyle` for code and documentation quality.

The analysis is executed at every push using the _gitlab ci_ platform.

## TODO

- [ ] Test on Windows
- [ ] Packaging for windows/mac/linux
- [ ] Improve undo/redo
- [ ] Complete descriptors
